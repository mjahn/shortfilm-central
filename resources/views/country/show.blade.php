@extends('layouts.app')

@section('title', 'Movie list')

@section('breadcrumb')
{{ Breadcrumbs::render('countries.show', $country) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <div class="columns">
	    <h1>{{ $country->name }}</h1>
	    <h2>@lang('country.movies')</h2>
	</div>
</div>
@include('movie.includes.movie_list', compact('movies'))
@endsection