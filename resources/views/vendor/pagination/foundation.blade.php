@if ($paginator->hasPages())
    <link rel="prefetch" href="{{ $paginator->previousPageUrl() }}">
    <link rel="prefetch" href="{{ $paginator->nextPageUrl() }}">
    <ul class="pagination text-center pagination-pointed" data-page="{{ $paginator->currentPage() }}" data-total="{{ $paginator->lastPage() }}">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="pagination-previous disabled">@lang('pagination.prev')</li>
        @else
            <li class="pagination-previous"><a class="pagination-pointed-button" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.prev')</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="ellipsis"></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="current active"><span>{{ $page }}</span></li>
                    @else
                        <li><a class="pagination-pointed-button" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pagination-next"><a class="pagination-pointed-button" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a></li>
        @else
            <li class="pagination-next disabled"><span>@lang('pagination.next')</span></li>
        @endif
    </ul>
@endif
