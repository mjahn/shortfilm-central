@extends('layouts.app')

@section('title', __('programs.index'))

@section('breadcrumb')
{{ Breadcrumbs::render('events.programs.index', $event) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
	@include('program.includes.program_list', compact('programs'))
@endsection
