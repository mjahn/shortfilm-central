<div class="cell">
    <div class="card" data-equalizer-watch>
        <div class="card-section">
            <a href="{{ $program->link() }}" title="{{ $program->name }}">
                {{ Str::limit($program->name, 50) }} 
            </a>
		    <p>{{ $program->description }}</p>
        </div>
    </div>
</div>
