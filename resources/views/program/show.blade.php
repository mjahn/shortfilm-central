@extends('layouts.app')

@section('title', $program->name)

@section('breadcrumb')
{{ Breadcrumbs::render('events.programs.show', $program, $event) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <header class="columns small-12">
		<h1>{{$program->name}}</h1>
    	@widget('programNavigator', compact('program'))        
    </header>
</div>
<div class="row">
    <div class="columns small-12">
		<dl><dt>Festival</dt><dd><a href="{{ $program->event->link() }}">{{ $program->event->name }}</a></dd></dl>
		<h2>@lang('program.movies')</h2>
		@include('movie.includes.movie_list', compact('movies'))
	</div>
</div>
@endsection