<div>
	<div class="grid-x grid-margin-x small-up-1 medium-up-2 large-up-4" data-equalizer data-equalize-on="medium">
	    @each('program.list_program', $programs, 'program')
	</div>
</div>
