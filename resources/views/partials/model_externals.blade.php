@foreach ($model->externals()->ofType('social')->get() as $external)
@endforeach

@if ($model->externals()->ofType('review')->exists())
<h4>Rezensionen</h4>
<div class="row small-up-1 medium-up-2 large-up-3">
@foreach ($model->externals()->ofType('review')->get() as $external)
	<div class="column">
		<a href="{{ $external->value }}" target="_blank">{{ $external->getFirstMedia('screenshot') }} {{$external->name }}</a>
	</div>
@endforeach
</div>
@endif

@if ($model->externals()->ofType('interview')->exists())
<h4>Interviews</h4>
<div class="row small-up-1 medium-up-2 large-up-3">
@foreach ($model->externals()->ofType('interview')->get() as $external)
	<div class="column">
		<a href="{{ $external->value }}" target="_blank">{{ $external->getFirstMedia('screenshot') }} {{$external->name }}</a>
	</div>
@endforeach
</div>
@endif

@if ($model->externals()->ofType('link')->exists())
<h4>Externe Seiten</h4>
<div class="row small-up-1 medium-up-2 large-up-3">
@foreach ($model->externals()->ofType('link')->get() as $external)
	<div class="column">
		<a href="{{ $external->value }}" target="_blank">{{ $external->getFirstMedia('screenshot') }} {{$external->name }}</a>
	</div>
@endforeach
</div>
@endif