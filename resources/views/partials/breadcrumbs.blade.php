@if (count($breadcrumbs))
    <nav aria-label="@lang('breadcrumbs.youarehere'):" role="navigation" class="row">
        <ul class="breadcrumbs columns">
            @foreach ($breadcrumbs as $breadcrumb)

                @if ($loop->last)
                    <li class="current"><span class="show-for-sr">@lang('breadcrumbs.current'):</span> {{ $breadcrumb->title }}</li>
                @elseif ($breadcrumb->url)
                    <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                @else
                    <li class="disabled">{{ $breadcrumb->title }}</li>
                @endif

            @endforeach
        </ul>
    </nav>

@endif
