@if (strpos($src, 'youtube.com/watch?v=') > 0)
<iframe src="{{ str_replace(['www.youtube.com/watch?v=', 'youtube.com/watch?v=' ], 'youtube.com/embed/', $src) }}" srcdoc="<style>*{padding:0;margin:0;overflow:hidden}html,body{height:100%}img,span{position:absolute;width:100%;top:0;bottom:0;margin:auto}span{height:1.5em;text-align:center;font:48px/1.5 sans-serif;color:white;text-shadow:0 0 0.5em black}</style><a href={{ str_replace(['www.youtube.com/watch?v=', 'youtube.com/watch?v=' ], 'youtube.com/embed/', $src) }}?autoplay=1><img src={{ str_replace(['www.youtube.com/watch?v=', 'youtube.com/watch?v=' ], 'img.youtube.com/vi/', $src) }}/hqdefault.jpg><span>▶</span></a>" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
@endif
@if (strpos($src, 'vimeo.com') > 0)
<iframe src="{{ str_replace([ 'www.vimeo.com/', 'vimeo.com/' ], 'player.vimeo.com/video/', $src) }}" allow="fullscreen" allowfullscreen></iframe>
@endif
