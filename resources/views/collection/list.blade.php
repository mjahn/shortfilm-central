@extends('layouts.app')

@section('title', __('collection.headers'))

@section('breadcrumb')
{{ Breadcrumbs::render('collections.index') }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
	<div class="columns">
		<h1>@lang('collection.headers')</h1>
		<h2>@lang('collection.my_headers')</h2>
	    @each('collection.includes.list_collection', $collections->get('my_collections'), 'collection')
		<h2>@lang('collection.public_headers')</h2>
	    @each('collection.includes.list_collection', $collections->get('public_collections'), 'collection')
	</div>
</div>
@endsection