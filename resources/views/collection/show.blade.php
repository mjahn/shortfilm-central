@extends('layouts.app')

@section('title')
{{ $collection->name }}
@endsection

@section('breadcrumb')
{{ Breadcrumbs::render('collections.show', $collection) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
	<h2>@lang('collection.collectables')</h2>
	@include('collection.includes.collectables_list', [ 'collectables' => $collection->collectables->sortBy('name') ])
@endsection