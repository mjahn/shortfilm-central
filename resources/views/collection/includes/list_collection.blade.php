<div class="panel panel-default">
  <div class="panel-heading"><a href="{{ $collection->link() }}">{{ $collection->name }}</a></div>
  <div class="panel-body">
    <p>{{ $collection->collectables->count() }} Einträge</p>
  </div>
</div>
