@extends('layouts.app')

@section('title', 'Personen')

@section('breadcrumb')
{{ Breadcrumbs::render('people.index') }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
	<div class="columns small-12">
		<h1>@lang('person.header')</h1>
		@include('person.includes.person_list', compact('people'))
	</div>
</div>
@endsection
