@extends('layouts.app')

@section('title'){{$person->name}}@endsection

@section('breadcrumb')
{{ Breadcrumbs::render('people.show', $person) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <header class="columns small-12">
        <h1>{{ $person->name }}<small> ({{ $person->year_of_birth ?? ''}})</small></h1>
    </header>
</div>
<div class="row">
    <div class="columns small-12 medium-3 large-2">
        <span class="thumbnail">{{ $person->getFirstMedia('images') }}</span>
    </div>
    <div class="columns">
        <dl>
            <dt>@lang('person.country')</dt><dd>{{ countryName($person->country_code) }}</dd>
        </dl>
    </div>
</div>
<div class="row">
    <div class="columns small-12">
        {{$person->bio }}
    </div>
</div>
<div class="row">
    <div class="columns small-12">
        <h3>Externe Links</h3>
        @include('partials.model_externals', [ 'model' => $person ])
    </div>
</div>
<div class="row">
    <div class="columns small-12">
        <h3>Filmographie</h3>
    @foreach ($casts as $roleCasts)
        <h4>{{ __('roles.' . $roleCasts->first()->castType()->first()->name) }}</h4>
        <ul class="no-bullet">
        @foreach ($roleCasts as $cast)
            <li>
                <span class="float-right">{{ $cast->movies()->first()->year }}</span>
                <strong><a href="{{ $cast->movies()->first()->link() }}">{{ $cast->movies()->first()->title }}</a></strong><br />
                <small>{{ $cast->name }}</small>
            </li>
        @endforeach
        </ul>
    @endforeach
    </div>
</div>
@endsection