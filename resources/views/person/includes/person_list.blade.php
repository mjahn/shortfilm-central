<nav aria-label="Pagination">
    {{ $people->links() }}
</nav>
<div class="row small-up-1 medium-up-3 large-up-6" data-equalizer data-equalize-on="medium">
    @each('person.list_person', $people, 'person')
</div>
<nav aria-label="Pagination">
    {{ $people->links() }}
</nav>
