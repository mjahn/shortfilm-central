<div class="columns">
    <div class="card" data-equalizer-watch>
        <div class="card-divider">
            <a href="{{ $person->link() }}">
                {!! $person->name !!} 
            </a>
        </div>
        <div class="card-section">
            {!! $person->description !!}
        </div>
    </div>
</div>
