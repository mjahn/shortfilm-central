<div class="panel panel-default">
    <h2 class="panel-heading">
        <a data-toggle="collapse{{ $key }}" data-parent="#accordion" href="#collapse{{ $key }}">{{ $key }}</a>
    </h2>
    <div id="collapse{{ $key }}" class="panel-collapse collapse">
        <ul class="panel-body">
            @each('person.casts_list', $casts, 'cast')
        </ul>
    </div>
</div>
