@extends('layouts.app')

@section('title', __('Not Found'))

@section('content')
	<h2>@lang('Not Found')</h2>
	{{ __($exception->getMessage() ?: 'Not Found') }}
@endsection