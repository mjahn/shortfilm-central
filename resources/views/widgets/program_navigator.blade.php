<nav aria-label="Pagination">
  <ul class="pagination">
    <li class="pagination-previous">
  	@if ($prev)
    	<a hreF="{{ $prev->link() }}">{{ $prev->name }}</a>
	@endif
    </li>
	<li class="current">{{$current->name}}</li>
    <li class="pagination-next">
  	@if ($next)
    	<a href="{{ $next->link() }}">{{ $next->name }}</a>
    @endif
    </li>
  </ul>
</nav>