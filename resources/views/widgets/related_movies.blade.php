@forelse ($movies as $movie)
    <div class="columns">
		<div class="card" data-equalizer-watch>
			<a href="{{ $movie->link() }}" class="card-cover">
				{{ $movie->getFirstMedia('images') }}
			</a>
			<div class="card-section">
				<h5>
					<a href="{{ $movie->link() }}">
						{!! $movie->title !!} 
					</a>
					<small>({{ $movie->year }})</small>
				</h5>
	            <div class="truncate-overflow">
	                {!! $movie->description !!}
	            </div>
			</div>
		</div>
	</div>
@empty
	<div class="columns auto">@lang('related_movies.empty')</div>
@endforelse
