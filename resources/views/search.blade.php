@extends('layouts.app')

@section('title', __('search.title', [ 'term' => $searchterm ]));

@section('breadcrumbs')

@endsection

@section('content')
<div class="row">
	<div class="columns">
		<h1>@lang('search.header')</h1>

		<p>
			@lang('search.excerpt', [ 'count' => $searchResults['count'], 'term' => $searchterm ])
		</p>
		<h2>Festivals</h2>
		<ul>
		@foreach($searchResults['events'] as $searchResult)
			<li><a href="{{ $searchResult->link() }}">{!! $searchResult->title !!}</a></li>
		@endforeach
		</ul>
		<h2>Filme</h2>
		<ul>
		@foreach($searchResults['movies'] as $searchResult)
			<li><a href="{{ $searchResult->link() }}">{!! $searchResult->title !!}</a></li>
		@endforeach
		</ul>
		<h2>Personen</h2>
		<ul>
		@foreach($searchResults['people'] as $searchResult)
			<li><a href="{{ $searchResult->link() }}">{!! $searchResult->name !!}</a></li>
		@endforeach
		</ul>
		<h2>Videos</h2>
		<ul>
		@foreach($searchResults['videos'] as $searchResult)
			<li><a href="{{ $searchResult->link() }}">{!! $searchResult->name !!}</a></li>
		@endforeach
		</ul>
	</div>
</div>
@endsection
