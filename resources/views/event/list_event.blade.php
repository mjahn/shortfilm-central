<div class="panel panel-default">
  <div class="panel-heading"><a href="{{ $event->link() }}">{{ $event->title }} ({{ $event->start_at->year }})</a></div>
  <div class="panel-body">
    <p>{{ $event->start() }} - {{ $event->end() }}</small></p>
    <p>{{ $event->description }}</p>
  </div>
</div>
