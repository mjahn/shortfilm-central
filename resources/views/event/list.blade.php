@extends('layouts.app')

@section('title', __('eventlist.header'))

@section('breadcrumb')
{{ Breadcrumbs::render('events.index') }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
	<div class="columns">
		<h1>@lang('eventlist.header')</h1>
	    <div class="center-block">{{ $events->links() }}</div>
	    @each('event.list_event', $events, 'event')
	    <div class="center-block">{{ $events->links() }}</div>
	</div>
</div>
@endsection