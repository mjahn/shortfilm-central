@extends('layouts.app')

@section('title')
{{ $event->name }}
@endsection

@section('breadcrumb')/
{{ Breadcrumbs::render('events.show', $event) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <div class="columns">
		<h1>{{ $event->name}}</h1>
	</div>
</div>
<div class="row">
    <div class="columns small-12 medium-3 large-2">
        <span class="thumbnail">{{ $event->getFirstMedia('images') }}</span>
    </div>
    <div class="columns">
		{{ $event->description }}
	</div>
</div>
<div class="row">
    <div class="columns small-12">
		<h2>@lang('event.programs')</h2>
		@include('program.includes.program_list', [ 'programs' => $event->programs->sortBy('name') ])
		<h2>@lang('event.movies')</h2>
		@include('movie.includes.movie_list', compact('movies'))
	</div>
</div>
@endsection