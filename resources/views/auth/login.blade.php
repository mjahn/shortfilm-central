@extends('layouts.app')

@section('title', __('auth.login_header'))

@section('breadcrumb')
{{ Breadcrumbs::render('auth.login') }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <div class="form-container small-12 medium-6 small-centered columns">
        <h1>@lang('auth.login_header')</h1>

        <form class="login-form" method="post" action="{{ route('login') }}">

            {{ csrf_field() }}

            <div class="email">
                <label for="email">@lang('E-Mail Address')</label>

                <input id="email" type="email" name="email" value="{{ old('email') }}" aria-describedby="emailHelpText" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-text" id="emailHelpText">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="password">
                <label for="password">@lang('Password')</label>

                <input id="password" type="password" name="password" aria-describedby="passwordHelpText" required>

                @if ($errors->has('password'))
                    <span class="help-text" id="passwordHelpText">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="checkbox">
                <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('Remember Me')
                </label>
            </div>

            <div class="button-plus-link">
                <button type="submit" class="button">
                    @lang('Login')
                </button>
                &nbsp;  
                <a href="{{ route('password.request') }}">
                    @lang('Forgot Your Password?')
                </a>
            </div>
        </form>
    </div>
</div>
@endsection
