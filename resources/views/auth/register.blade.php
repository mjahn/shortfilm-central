@extends('layouts.app')

@section('title', __('auth.register_header'))

@section('breadcrumb')
{{ Breadcrumbs::render('auth.register') }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <div class="form-container small-12 medium-6 small-centered columns">
        <h1>@lang('auth.register_header')</h1>
        <form class="register-form" method="post" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="name">
                <label for="email">@lang('Name')</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" aria-describedby="nameHelpText" required autofocus>
                @if ($errors->has('name'))
                    <span class="help-text" id="nameHelpText">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="email">
                <label for="email">@lang('E-Mail Address')</label>
                <input id="email" type="email" name="email" value="{{ old('email') }}" aria-describedby="emailHelpText" required>
                @if ($errors->has('email'))
                    <span class="help-text" id="emailHelpText">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="password">
                <label for="password">@lang('Password')</label>
                <input id="password" type="password" name="password" aria-describedby="passwordHelpText" required>
                @if ($errors->has('password'))
                    <span class="help-text" id="passwordHelpText">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="password-confirm">
                <label for="password-confirm">@lang('Confirm Password')</label>
                <input id="password-confirm" type="password" name="password_confirmation" required>
            </div>

            <div class="register_button">
                <button type="submit" class="button">@lang('Register')</button>
            </div>
        </form>
    </div>
</div>

@endsection
