@extends('layouts.app')

@section('title', 'Movie list')

@section('breadcrumb')
{{ Breadcrumbs::render('movies.index') }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
    <div class="columns">
		<h1>
			@lang('movie.header_list')
			<small>
				<a data-toggle="filter">Filter anzeigen</a>
			</small>
		</h1>
		<aside data-toggler id="filter">	
xycdsfsdfs
		</aside>
		@if (count($filter) > 0)
		<p>
			<strong>@lang('movie.filter_label'):</strong>
			@foreach ($filter as $name => $value)
				{!! __('filter.type_'.$name, [ 'value' => $value ]) !!}
			@endforeach
		</p>
		@endif
	</div>
</div>
@include('movie.includes.movie_list', compact('movies'))
@endsection
