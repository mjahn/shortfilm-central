    <li>
    	<a href="{{ $data[0]->link() }}">{{ $data[0]->name }}</a> ({{ Lang::choice(__('event.program_list'), count($data[1])) }}: {!! $data[1]->implode('link', ', ') !!})
    </li>
