    <div class="image-preview-link embed-container">
		<a href="#" data-open="modal-gallery">
        	{{ $poster }}
    	</a>
    </div>
@foreach ($images as $media)
    <div class="image-preview-link embed-container">
		<a href="#gallery-image-{{ $media->id }}" data-open="modal-gallery">
        	{{ $media }}
    	</a>
    </div>
@endforeach

