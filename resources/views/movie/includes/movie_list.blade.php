<div class="row">
	<nav aria-label="Pagination" class="columns">
    	{{ $movies->links() }}
	</nav>
</div>
<div class="row small-up-1 medium-up-3 large-up-6" data-equalizer data-equalize-on="medium">
    @each('movie.list_movie', $movies, 'movie')
</div>
<div class="row">
	<nav aria-label="Pagination" class="columns">
    	{{ $movies->links() }}
	</nav>
</div>
