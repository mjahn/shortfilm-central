@foreach ($videos as $video)
    <div class="video-preview-link embed-container">
		<a href="#" data-open="modal-video-{{ $video->path }}">
	    	<img src="https://drive.google.com/thumbnail?authuser=0&sz=w320&id={{ $video->path }}" />
			<span>▶</span>
		</a>
    </div>
@endforeach

@foreach ($videos as $video)
<div id="modal-video-{{ $video->path }}" class="large reveal" data-reveal="" data-reset-on-close="true">
    <iframe src="https://drive.google.com/file/d/{{ $video->basename }}/preview" allowfullscreen></iframe>
</div>
@endforeach
