    <li style="background: url('{{ $movie->poster }}') no-repeat; background-size: 100% auto; width: 10em; height: 15em;">
        <a href="./movies/{{ $movie->id }}">
            {{ $movie->name }} ({{ $movie->year }}, @each('movie.list_country_link', $movie->countries, 'country'))
        </a>
    </li>
