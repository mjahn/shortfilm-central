@extends('layouts.app')

@section('title', $movie->title . ' (' . $movie->year . ')')

@section('breadcrumb')
{{ Breadcrumbs::render('movies.show', $movie) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('meta-description', $movie->description)

@section('content')
<article>
    <div class="row">
        <div class="columns">
            <h1>{{ $movie->title }} <small>({{ $movie->year }})</small></h1>
        </div>
    </div>
    <div class="row">
        <div class="columns small-12 medium-4 large-3">
            <span class="thumbnail">{{ $movie->poster }}</span>
        </div>
        <div class="columns">
            <dl class="meta clearfix">
                <dt>@lang('movie.year')</dt><dd><a href="{{ route('movies.index') }}?filter[year]={{ $movie->year }}">{{ $movie->year }}</a>&nbsp;</dd>
                <dt>@lang('movie.length')</dt><dd>{{ $movie->length }}&nbsp;</dd>
                <dt>@lang('movie.countries')</dt><dd><ul class="inline comma">@each('movie.list_country_link', $movie->countries, 'country')</ul>&nbsp;</dd>
                <dt>@lang('movie.festivals')</dt><dd><ul class="inline comma">@each('movie.list_event_program_link', $events, 'data')</ul>&nbsp;</dd>
                <dt>@lang('movie.categories')</dt><dd><ul class="inline comma">@each('movie.list_category_link', $movie->categories, 'category')</ul>&nbsp;</dd>
            </dl>
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <p>{!! trim($movie->description) !!}</p>

            <h2>@lang('movie.images_videos')</h2>
            <div id="gallery" data-slick='{"variableWidth": true, "arrows": true, "slidesToShow": 6, "slidesToScroll": 1} '>
            @foreach ($movie->drive_videos as $video)
                <div class="video-preview-link embed-container">
                    <a href="#" onclick="gotoSlide({{$loop->index}})" data-open="modal-gallery">
                        {!! $video->preview_image !!}
                        <span>▶</span>
                        {{$video->path}}
                    </a>
                </div>
            @endforeach
                <div class="image-preview-link embed-container">
                    <a href="#" onclick="gotoSlide({{$movie->drive_videos->count()}})" data-open="modal-gallery">
                        {{ $movie->poster }}
                    </a>
                </div>
            @foreach ($movie->images as $media)
                <div class="image-preview-link embed-container">
                    <a href="#" onclick="gotoSlide({{$movie->drive_videos->count() + 1 + $loop->index}})" data-open="modal-gallery">
                        {{ $media }}
                    </a>
                </div>
            @endforeach
            @foreach ($movie->external_videos as $external)
                <div class="{{ $external->externalType()->first()->name}}-preview-link embed-container">
                    <a href="#" onclick="gotoSlide({{$movie->drive_videos->count() + 1 + $movie->images->count() + $loop->index}})" data-open="modal-gallery">
                        {!! $external->preview_image !!}
                        <span>▶</span>
                    </a>
                </div>
            @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="columns small-12 medium-6">
            <h2>@lang('movie.cast')</h2>

            <dl class="clearfix">
            @foreach ($movie->casts->groupByModel('castType') as $data)
                <dt>{{__('roles.' . $data[0]->name)}}</dt>
                <dd>
                    <ul class="inline comma">
                    @foreach ($data[1] as $cast)
                        <li>
                            <a href="{{ $cast->person->link() }}">{{$cast->person->name}}</a>
                        @if (trim($cast->name) !== '')
                            ({{$cast->name}})
                        @endif
                        </li>
                    @endforeach
                    </ul>
                </dd>
            @endforeach
            </dl>
        </div>
        <div class="columns small-12 medium-6">
            @include('partials.model_externals', [ 'model' => $movie ])
        </div>
    </div>
    <div class="row">
        <div class="columns">
            <h2>@lang('movie.related_movies_header')</h2>
            <div class="row small-up-1 medium-up-3 large-up-6" data-equalizer data-equalize-on="medium">
                @widget('relatedMovies', compact('movie'))        
            </div>
        </div>
    </div> 
</article>
@endsection

@section('bottom')

    <div id="modal-gallery" class="reveal large" data-reveal data-reset-on-close="true">
        <div data-slick='{"slidesToShow": 1, "slidesToScroll": 1, "adaptiveHeight": true}'>
        @foreach ($movie->drive_videos as $video)
            <div class="embed-container">
                <iframe src="https://drive.google.com/file/d/{{ $video->path }}/preview" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        @endforeach
            <div>
                {{ $movie->poster }}
            </div>
        @foreach ($movie->images as $media)
            <div>
                {{ $media }}
            </div>
        @endforeach
        @foreach ($movie->external_videos as $external)
            <div class="embed-container">
                <iframe src="{{ $external->embed }}" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        @endforeach
        </div>
    </div>
@endsection

@push('scripts')
<script>
function gotoSlide(slide) {
    $('modal-gallery').slick('slickGoTo', slide);
}
</script>
@endpush