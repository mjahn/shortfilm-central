<div class="columns">
    <div class="card" data-equalizer-watch>
        <a href="{{ $movie->link() }}" class="card-cover">
            {{ $movie->poster }}
        </a>
        <div class="card-section">
            <h4>
                <a href="{{ $movie->link() }}" title="{{ $movie->title }}">
                    {{ Str::limit($movie->title, 15) }} 
                </a>
            </h4>
            <small class="text-muted">
                {{ $movie->year }} / <ul class="inline comma">@each('movie.list_country_link', $movie->countries, 'country')</ul>
            </small>
            <div class="truncate-overflow">
                {!! $movie->description !!}
            </div>
        </div>
    </div>
</div>