@extends('layouts.app')

@section('title', 'Shortfilm Central')

@section('sidebar')
    @parent
@endsection

@section('content')
    <ul style="overflow: hidden;">
    @each('movie.list_movie', $movies, 'movie')
    </ul>
@endsection