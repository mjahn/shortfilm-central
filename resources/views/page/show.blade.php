@extends('layouts.app')

@section('title', $page->title)

@section('breadcrumb')
{{ Breadcrumbs::render('pages.show', $page) }}
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
<div class="row">
	<div class="columns">
		<h1>{{ $page->title }}</h1>
		{!! $page->body !!}
	</div>
</div>
@endsection