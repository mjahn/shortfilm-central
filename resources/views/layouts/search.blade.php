<li>
	<form action="{{ route('search') }}" method="get" role="search">
		<input id="search" name="search" type="text" data-autocomplete data-autocomplete-remote="{{ route('search.autocomplete') }}" placeholder="@lang('search.placeholder')" />
	</form>
</li>