<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{ str_limit($__env->yieldContent('meta-description', ''), 320) }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - {{ config('app.name', 'short matters') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="page">
        @include('maintenancemode::notification')
        @include('layouts.navigation')
        <main>
            @yield('breadcrumb')
            @yield('content')
        </main>
        @include('layouts.footer')
    </div>
    @yield('bottom')
    <script src="{{ mix('/js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>
