<div class="title-bar flex-container align-spaced" data-responsive-toggle="main-menu" data-hide-for="medium">
	<button class="menu-icon flex-child-shrink" type="button" data-toggle="main-menu"></button>
	<ul class="menu dropdown" id="user-menu" data-dropdown-menu>
		@include('layouts.search')
		{{ Menu::user() }}
	</ul>
</div>
<div class="top-bar">
	<div class="top-bar-left">
		{{ Menu::main() }}
	</div>
	<div class="top-bar-right">
		<ul class="vertical medium-horizontal menu vertical menu accordion-menu hide-for-small-only" data-responsive-menu="accordion medium-dropdown" id="user-menu">
			@include('layouts.search')
			{{ Menu::user() }}
		</ul>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			{{ csrf_field() }}
		</form>
	</div>
</div>