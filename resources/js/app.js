/**
* First we will load all of this project's JavaScript dependencies which
* includes Vue and other libraries. It is a great starting point when
* building robust, powerful web applications using Vue and Laravel.
*/
require('./bootstrap');
require('slick-carousel');

$(document).ready(function(){
    $(document).foundation();

    $('[data-autocomplete]').each(function (index) {
        var element = $(this);
        element.autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: element.attr('data-autocomplete-remote'),
                    data: {
                        term : request.term
                    },
                    dataType: 'json',
                    success: function (data){
                        var resp = $.map(data,function(obj) {
                            return obj.name;
                        }); 
                        response(resp);
                    }
                });
            },
            minLength: 2,
        });
    });

    $('[data-slick]').each(function (index) {
        var element = $(this);
    
        if (element.parents('.reveal').length > 0) {
            // slick slider inside a modal
            $(element.parents('.reveal')[0]).on('open.zf.reveal', function() {
                initSlickSlider(element);
            });
            $(element.parents('.reveal')[0]).on('closed.zf.reveal', function() {
                element.unslick();
            });
        } else {
            initSlickSlider(element);
        }
    });


    function initSlickSlider(element) {
        $(element).slick({
            infinite: true,
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                }, {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    } 
});
