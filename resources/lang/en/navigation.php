<?php

return array (
  'admin' => 'Manage',
  'administration' => 'Administration',
  'collections' => 'Collections',
  'countries' => 'Countries',
  'events' => 'Festivals',
  'home' => 'Begin',
  'login' => 'Login',
  'logout' => 'Logout',
  'movies' => 'Movies',
  'movies_by_category' => 'Movies by genre',
  'movies_by_year' => 'Movies by year',
  'people' => 'People',
  'programs' => 'Programs',
  'register' => 'Register',
  'translations' => 'Translationtool',
  'userprofile' => 'Your profile',
);
