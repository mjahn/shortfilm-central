<?php

return array (
  'header' => 'Search results',
  'title' => 'Search results for the term ":term"',
  'excerpt' => ':count results for the searchterm ":term"',
  'placeholder' => 'Search',
);
