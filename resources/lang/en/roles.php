<?php

return array (
  'actor' => 'Actor',
  'camera' => 'Cinematographer',
  'director' => 'Director',
  'editor' => 'Editor',
  'music' => 'Music',
  'producer' => 'Producer',
  'production-design' => 'Production design',
  'screenplay' => 'Screenplay',
  'sound' => 'Sound',
  'animation' => 'Animation',
);
