<?php

return array (
  'header' => 'Collection: :name',
  'headers' => 'Collections',
  'my_headers' => 'My collections',
  'public_headers' => 'Public collections',
);
