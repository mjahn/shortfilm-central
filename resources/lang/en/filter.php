<?php

return [
	'type_starts_with' => 'First letter(s): <q>:value</q>',
	'type_category' => 'Genre: <q>:value</q>',
	'type_country' => 'Country: :value'
];
