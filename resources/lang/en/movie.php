<?php

return array (
  'header_images' => 'Movie images',
  'length' => 'Duration',
  'year' => 'Year',
  'cast' => 'Cast',
  'countries' => 'Countries',
  'festivals' => 'Festivals',
  'categories' => 'Genre',
  'filter_category' => 'by genre',
  'filter_label' => 'active filters',
  'filter_year' => 'by year',
  'header_list' => 'Movie list',
  'related_movies' => 'Related movies',
  'videos' => 'Videos for this movie',
);
