<?php

return array (
  'headers' => 'Sammlungen',
  'header' => 'Sammlung: :name',
  'public_headers' => 'Öffentliche Sammlungen',
  'my_headers' => 'Meine Sammlungen',
);
