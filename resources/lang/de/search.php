<?php

return array (
  'title' => 'Suche nach ":term"',
  'excerpt' => ':count Treffer für den Suchbegriff <q>:term</q>',
  'header' => 'Suchergebnisse',
  'placeholder' => 'Suchen',
);
