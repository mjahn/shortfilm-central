<?php

return [
	'type_starts_with' => 'Anfangsbuchstabe(n): <q>:value</q>',
	'type_category' => 'Genre: <q>:value</q>',
	'type_country' => 'Land: :value'
];
