<?php

return array (
  'header_images' => 'Bilder zum Film',
  'year' => 'Jahr',
  'length' => 'Länge',
  'countries' => 'Länder',
  'festivals' => 'Festivals',
  'cast' => 'Crew & Cast',
  'images_videos' => 'Bilder & Videos',
  'header_list' => 'Filmübersicht',
  'filter_label' => 'aktive Filter',
  'filter_year' => 'nach Jahr',
  'filter_category' => 'nach Genre',
  'categories' => 'Genre',
  'related_movies' => 'Ähnliche Filme',
  'videos' => 'Videos zu diesem Film',
  'related_movies_header' => 'Ähnliche Filme',
  'related_movies_empty' => 'Keine verwandten Filme gefunden',
);
