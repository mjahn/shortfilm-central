<?php

return array (
  'home' => 'Start',
  'movies' => 'Filme',
  'countries' => 'Länder',
  'events' => 'Festivals',
  'people' => 'Menschen',
  'admin' => 'Verwaltung',
  'translations' => 'Übersetzungstool',
  'administration' => 'Admintool',
  'login' => 'Anmelden',
  'register' => 'Registrieren',
  'programs' => 'Programme',
  'logout' => 'Abmelden',
  'userprofile' => 'Dein Benutzerprofil',
  'collections' => 'Deine Sammlungen',
  'movies_by_year' => 'Filme nach Jahr',
  'movies_by_category' => 'Filme nach Genres',
);
