<?php

return array (
  'director' => 'Regie',
  'camera' => 'Kamera',
  'producer' => 'Produzent',
  'screenplay' => 'Drehbuch',
  'editor' => 'Schnitt',
  'sound' => 'Ton',
  'actor' => 'Darsteller',
  'music' => 'Musik',
  'production-design' => 'Produktionsdesign',
  'animation' => 'Animation',
);
