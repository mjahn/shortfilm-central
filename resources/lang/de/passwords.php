<?php

return array (
  'password' => 'Das Passwort muss mindestens 8 Zeichen haben und die folgenden Bedingungen erfüllen.',
  'reset' => 'Dein Passwort wurde zurückgesetzt.',
  'sent' => 'Wir haben Dir einen Link zugesendet, mit dem Du Dein Passwort zurücksetzen kannst.',
  'token' => 'Das Token zum Zurücksetzen Deines Passwortes ist ungültig.',
  'user' => 'Zu dieser Emailadresse konnten wir keinen Account finden.',
);
