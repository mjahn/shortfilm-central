<?php

return array (
  'failed' => 'Wir konnten keinen Account mit diesen Logindaten finden.',
  'throttle' => 'Zu viele Loginversuche. Bitte versuche es in :seconds Sekunden noch einmal.',
  'login_header' => 'Einloggen',
  'register_header' => 'Registrieren',
);
