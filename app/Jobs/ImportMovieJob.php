<?php

namespace App\Jobs;

use App\Jobs\DownloadFestivalMovie;
use App\Models\ImportMovie;
use App\Models\Cast;
use App\Models\CastType;
use App\Models\Category;
use App\Models\Country;
use App\Models\Event;
use App\Models\EventProgram;
use App\Models\External;
use App\Models\ExternalType;
use App\Models\Movie;
use App\Models\Person;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportMovieJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

	/**
	 * The number of seconds to wait before retrying the job.
	 *
	 * @var int
	 */
	public $retryAfter = 600;

	/**
	* The number of times the job may be attempted.
	*
	* @var int
	*/
	public $tries = 300;

	/**
	 * movie-data instance
	 * @var \App\Models\ImportMovie
	 */
	protected $importMovie;

	/**
	 * Create a new job instance.
	 *
	 * @param \App\Models\ImportMovie $importMovie instance of ImportMovie with the movie-data
	 * @return void
	 */
	public function __construct(ImportMovie $importMovie)
	{
		$this->importMovie = $importMovie;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$event = null;
		$programs = collect([]);
        $countries = collect([]);
		$casts = collect([]);
        $people = collect([]);
        $externals = collect([]);
		$categories = collect([]);
        $this->prepareCountryMap();

		$movieData = $this->importMovie;
        // print_r($movieData);
		// create event
		// create program in event
		if ($movieData->event) {
			$event = $this->findOrCreateEvent($movieData->event);
            if (!is_array($movieData->programs)) {
                $movieData->programs = explode(',', $movieData->programs);
            }
            foreach (array_wrap($movieData->programs) as $programName) {
                $programName = html_entity_decode(trim($programName));
                $programs->push($this->findOrCreateEventProgram($event, $programName));
            }
		}

		// create category
		if ($movieData->category) {
            if (!is_array($movieData->category)) {
                $data = explode(',', $movieData->category);
            } else {
                $data = $movieData->category;
            }
            foreach ($data as $category) {
                $category = html_entity_decode(trim($category));
                $categories->push($this->findOrCreateCategory($category));
            }
		}

        // add countries
        if ($movieData->countries) {
            foreach (array_wrap($movieData->countries) as $countryName) {
                $countryNames = explode('/', $countryName);
                foreach ($countryNames as $name) {
                    if (trim($name) === '') {
                        continue;
                    }
                    $name = html_entity_decode(trim($name));
                    $countries->push($this->findOrFailCountry($name));
                }
            }
        }

		// create casts
        if ($movieData->casts) {
            foreach ($movieData->casts as $castType => $peopleNames) {
                $castType = $this->findOrCreateCastType($castType);
                
                foreach (array_wrap($peopleNames) as $personNames) {
                    $personNames = str_replace(' & ', ', ', $personNames);
                    $data = explode(',', $personNames);
                    foreach ($data as $personName) {
                        $personName = trim($personName);                    
                        $personName = html_entity_decode(trim($personName));

                        if (isset($countries[0])) {
                            $person = $this->findOrCreatePerson($personName, $castType, $countries[0]->iso_3166_2);
                        } else {
                            $person = $this->findOrCreatePerson($personName, $castType);
                        }

                        $imageKey = $castType . '_image';
                        if (optional($movieData)->$imageKey) {
                            $this->addMedia($person, $movieData->$imageKey, 'images', $personName);
                        }
                        $descKey = $castType . '_description';
                        if (optional($movieData)->$descKey) {
                            $person->bio = $movieData->$descKey;
                        }
                        $casts->push($this->findOrCreateCast($person, $castType, ''));
                        $people->push($person);
                    }
                }
            }

        }
        $movieData->description = html_entity_decode(trim(strip_tags($movieData->description)));
        $movieData->title = html_entity_decode(trim($movieData->title));
        $movieData->title_english = html_entity_decode(trim($movieData->title_english));
        $movieData->year = str_replace('N/A', '0', $movieData->year);
        
        // get movie
		$movie = Movie::whereTranslation('name', $movieData->title)->where('year', '=', $movieData->year)->first();

        // create movie
        if ($movie === null) {
            $movie = new Movie();
            $movie->name = $movieData->title;
            $movie->description = $movieData->description;
            $movie->year = $movieData->year;
            $movie->length = $movieData->length;
            $movie->save();
        }

        $director = '';
        if (isset($movieData->casts['director'])) {
            $director = $movieData->casts['director'];
            if (is_array($director)) {
                $director = join(', ', $director);
            }
        }

        if (isset($movieData->title_english) && $movieData->title_english) {
            $movie->setTranslation('name', 'en', $movieData->title_english);
        }

        if (isset($movieData->title_german) && $movieData->title_german) {
            $movie->setTranslation('name', 'de', $movieData->title_german);
            foreach ($contries as $country) {
                echo 'Translate title to ' . strtolower($country->iso_3166_2) . "\n";
                $movie->setTranslation('name', strtolower($country->iso_3166_2), $movieData->title);
            }
        }

        if ($movieData->externals && is_array($movieData->externals)) {
            foreach ($movieData->externals as $url) {
                $externals->push($this->findOrCreateExternal($movie, $url, ExternalType::where('name', '=', 'link')->first()));
            }
        }

        if ($movieData->trailer && is_array($movieData->trailer)) {
            foreach ($movieData->trailer as $url) {
                $externals->push($this->findOrCreateExternal($movie, $url, ExternalType::where('name', '=', 'video')->first()));
            }
        }

        if ($movieData->video) {
            dispatch(new DownloadFestivalMovie($movieData->video, $movieData->title, $movieData->year, $director, $movieData->event))->onQueue('download');
        }
        $imageTitle = $movieData->title . ' (' . $movieData->year . ') ' . $director;

        if ($movieData->image) {
            if (!is_array($movieData->image)) {
                $movieData->image = [ $movieData->image ];
            }
            foreach ($movieData->image as $image) {
                $this->addMedia($movie, $image, 'images', $imageTitle);
            }
        }
        if ($movieData->poster) {
            if (!is_array($movieData->poster)) {
                $movieData->poster = [ $movieData->poster[0] ];
            }
            foreach ($movieData->poster as $poster) {
                $this->addMedia($movie, $poster, 'poster', $imageTitle);
            }
        }

        $movie->countries()->syncWithoutDetaching($countries->pluck('id'));
        $movie->categories()->syncWithoutDetaching($categories->pluck('id'));
        $movie->casts()->syncWithoutDetaching($casts->pluck('id'));
        $movie->events()->syncWithoutDetaching($event->id);
        $movie->eventPrograms()->syncWithoutDetaching($programs->pluck('id'));
        $movie->externals()->syncWithoutDetaching($externals->pluck('id'));

        $movie->save();

		// throw new Exception('Not yet implemented');
	}

    /**
     * Adds media to a model
     * 
     * @param [type] $model      [description]
     * @param [type] $url        [description]
     * @param [type] $collection [description]
     * @param [type] $title      [description]
     */
    protected function addMedia($model, $url, $collection, $title) 
    {
        $imageExtension = explode('?', $url)[0];
        $imageExtension = explode('.', $imageExtension);
        $imageExtension = array_pop($imageExtension);
        $filename = sha1($url) . '.' . $imageExtension;
        $title = substr($title, 0, 191);

        // check if media from this url already exists and stop adding this media
        foreach ($model->getMedia($collection) as $media) {
            if ($media->file_name === $filename) {
                return;
            }
        }

        echo 'Found media ' .  $url . ' -> store to ' . $filename . "\n";
        try {
            $media = $model
                ->addMediaFromUrl($url)
                ->usingFileName($filename)
                ->usingName($title)
                ->withResponsiveImages()
                ->toMediaCollection($collection);
        } catch (\Exception $e) {
            \Log::info('Exception', ['exception' => $e]);
        }
    }

	protected function findOrCreateEvent($eventName)
	{
        $event = Event::whereTranslation('name', $eventName)->first();
        if ($event) {
            return $event;
        }

        $event = new Event();
		$event->description = '';
        $event->event_type_id = 1;
        $event->start_at = now();
        $event->end_at = now();
        $event->country_code = 'DE';
        $event->save();

        return $event;
	}

	protected function findOrCreateEventProgram(Event $event, $programName)
	{
        return EventProgram::firstOrCreate([
            'name' => $programName,
            'event_id' => $event->id
        ], [
            'description' => '',
        ]);
	}

	protected function findOrCreatePerson($personName, $castType, $countryCode = null)
	{
        if ($countryCode) {
            return Person::firstOrCreate([
                'name' => $personName,
                'country_code' => $countryCode,
            ]);
        }
        return Person::firstOrCreate([
            'name' => $personName,
        ],  [
            'country_code' => '',
        ]);
	}

    protected function findOrCreateCast(Person $person, CastType $castType, $name = '')
    {
        return Cast::firstOrCreate([
            'person_id' => $person->id,
            'cast_type_id' => $castType->id,
        ], [
            'name' => $name,
        ]);
    }

    protected function findOrCreateCategory($categoryName)
    {
        switch (strtolower($categoryName)) {
            case 'spielfilm':
                $categoryName = 'Fiction';
                break;
            case 'animation':
                $categoryName = 'Animation';
                break;
        }

        $category = Category::whereTranslation('name', $categoryName)->first();

        if ($category) {
            return $category;
        }

        $category = new Category();
        $category->name = $categoryName;
        $category->save();

        return $category;        
    }

    protected function findOrCreateCastType($name)
    {
        return CastType::firstOrCreate([
            'name' => $name
        ]);
    }

    protected function findOrCreateExternal($movie, $url, $type)
    {
        return External::firstOrCreate([
            'movie_id' => $movie->id,
            'value' => $url,
            'external_type_id' => $type->id,
        ], [
            'name' => '',
        ]);
    }

	protected function findOrCreateMovie($movieName, $movieYear, $directorName)
	{
        return Movie::firstOrCreate([
            'name' => $movieName,
            'year' => $movieYear,
        ]);
	}

	protected function findOrFailCountry($name)
	{
        $name = trim($name);

        if (isset ($this->countryMap[$name])) {
            $name = $this->countryMap[$name];
        }
        // $name = str_replace($this->searchCountries, $this->replaceCountries, $name);

        \Log::info('Try to find country "' . $name . '"');
        $country = Country::where('name', '=', $name)
            ->orWhere('full_name', '=', $name)
            ->orWhere('iso_3166_2', '=', $name)
            ->orWhere('iso_3166_3', '=', $name)
            ->firstOrFail();
        return $country;
    }

    protected function prepareCountryMap()
    {
        $this->countryMap = [
            'Deutschland' => 'Germany',
            'Griechenland' => 'Greece',
            'Serbien' => 'Serbia',
            'Serbien und Montenegro' => 'Serbia',
            'Österreich' => 'Austria',
            'Schweiz' => 'Switzerland',
            'Frankreich' => 'France',
            'Norwegen' => 'Norway',
            'Niederlande' => 'Netherlands',
            'Großbritannien' => 'United Kingdom',
            'England' => 'United Kingdom',
            'Kenia' => 'Kenya',
            'Australien' => 'Australia',
            'Belgien' => 'Belgium',
            'Brasilien' => 'Brazil',
            'Dänemark' => 'Denmark',
            'Georgien' => 'Georgia',
            'Georgie' => 'Georgia',
            'Indien' => 'India',
            'Iran' => 'Islamic Republic of Iran',
            'Italien' => 'Italy',
            'Kanada' => 'Canada',
            'Kosovo' => 'Serbia',
            'Libanon' => 'Lebanon',
            'Mexiko' => 'Mexico',
            'Netherlandsn' => 'Netherlands',
            'Neuseeland' => 'New Zealand',
            'Oesterreich' => 'Austria',
            'Polen' => 'Poland',
            'Rumänien' => 'Romania',
            'Russland' => 'Russian Federation',
            'Schweden' => 'Sweden',
            'Singapur' => 'Singapore',
            'Spanien' => 'Spain',
            'Tunesien' => 'Tunisia',
            'Turkei' => 'Turkey',
            'Ungarn' => 'Hungary',
            'Albanien' => 'Republic of Albania',
            'Argentinien' => 'Argentina',
            'Armenien' => 'Armenia',
            'Aserbaidschan' => 'Azerbaijan',
            'Bulgarien' => 'Bulgaria',
            'Dominikanische Republik' => 'Dominican Republic',
            'Estland' => 'Estonia',
            'Indonesien' => 'Indonesia',
            'Irak' => 'Iraq',
            'Irland' => 'Ireland',
            'Island' => 'Iceland',
            'Jordanien' => 'Jordan',
            'Kasachstan' => 'Kazakhstan',
            'Kirgisien' => 'Kyrgyzstan',
            'Kolumbien' => 'Colombia',
            'Kroatien' => 'Croatia',
            'Lettland' => 'Latvia',
            'Litauen' => 'Lithuania',
            'Luxemburg' => 'Luxembourg',
            'Macau' => 'Macao',
            'Marokko' => 'Morocco',
            'Moldawien' => 'Republic of Moldova',
            'Palestina' => 'Palestinian Territory, Occupied',
            'Ruanda' => 'Rwanda',
            'Slowakei' => 'Slovakia',
            'Slowenien' => 'Slovenia',
            'Syrien' => 'Syrian Arab Republic',
            'Süd Afrika' => 'South Africa',
            'Südkorea' => 'KOR',
            'South Korea' => 'KOR',
            'Taiwan' => 'Taiwan, Province of China',
            'Tansania' => 'United Republic of Tanzania',
            'Tschechien' => 'Czech Republic',
            'Venezuela' => 'Bolivarian Republic of Venezuela',
            'Vereinigte Arabische Emirate' => 'United Arab Emirates',
            'Vietnam' => 'Socialist Republic of Vietnam',
            'Weißrussland' => 'Belarus',
            'Zypern' => 'Cyprus',
            'Ägypten' => 'Arab Republic of Egypt',
            'The Netherlands' => 'Kingdom of the Netherlands',
            'Aserbajdschan' => 'Azerbaijan',
            'Bangladesch' => 'Bangladesh',
            'Bosnien und Herzegowina' => 'Bosnia and Herzegovina',
            'Bosnien-Herzegowina' => 'Bosnia and Herzegovina',
            'BRD'  => 'Germany',
            'DDR' => 'Germany',
            'Deutschand' => 'Germany',
            'DR Kongo' => 'Democratic Republic of the Congo',
            'England' => 'United Kingdom',
            'Finnland' => 'Finland',
            'Frankeich' => 'France',
            'Grönland' => 'Greenland',
            'Israel | Israel' => 'Israel',
            'Kamerun' => 'Cameroon',  
            'Kap Verde' => 'Cape Verde',
            'Katar' => 'Qatar',
            'KDR' => 'Republic of Korea',
            'Kirgisistan' => 'Kyrgyzstan',
            'Kirgistan' => 'Kyrgyzstan',
            'KOR' => 'Republic of Korea',
            'Kuba' => 'Cuba',
            'Kyrgyzstan' => 'Kyrgyzstan',
            'Madagaskar' => 'Madagascar',
            'Mazedonien' => 'the former Yugoslav Republic of Macedonia',
            'Moldau' => 'Republic of Moldova',
            'Mosambik' => 'Republic of Mozambique',
            'Mosambique' => 'Republic of Mozambique',  
            'Netherlands' => 'Kingdom of the Netherlands',
            'Neusseland' => 'New Zealand',
            'Palästinensische Autonomiegebiete' => 'Palestinian Territory, Occupied',  
            'Philippinen' => 'Republic of the Philippines', 
            'Republik Mazedonien' => 'the former Yugoslav Republic of Macedonia',
            'Republik Moldau' => 'Republic of Moldova',
            'Rumänen' => 'Romania',
            'Russia' => 'Russian Federation', 
            'Russian Federationn Federation' => 'Russian Federation', 
            'serbien' => 'Serbia', 
            'Simbabwe' => 'Zimbabwe',  
            'Slovenien' => 'Slovenia', 
            'Slovenija' => 'Slovenia',  
            'Slowakische Republik' => 'Slovakia',  
            'Sowjetunion' => 'Russian Federation',
            'Syrian Arab Republic' => 'Syrian Arab Republic',
            'Süd Korea' => 'Republic of Korea',
            'Südafrika' => 'South Africa',
            'Tadschikistan' => 'Tajikistan',
            'Tschechische Republik' => 'Czech Republic',
            'Tschechoslowakei' => 'Czech Republic',
            'Türkei' => 'Turkey',
            'UdSSR (Georgia)' => 'Georgia',
            'UdSSR (Ukraine)' => 'Ukraine', 
            'UdSSR' => 'Russian Federation', 
            'UK' => 'United Kingdom',
            'Ungar' => 'Hungary',
            'Usbekistan' => 'Uzbekistan',
            'Vereinigte Staaten' => 'USA', 
            'Zaire' => 'Democratic Republic of the Congo',
            'Äthiopien' => 'Federal Democratic Republic of Ethiopia',  
            'ČSSR' => 'Czech Republic',
            'Ethoiopia' => 'Federal Democratic Republic of Ethiopia',
            'Republik the former Yugoslav Republic of Macedonia' => 'the former Yugoslav Republic of Macedonia',
            'Russland (Tschetschenien)' => 'Russland',
        ];
        // $this->searchCountries = array_keys($countryMap);
        // $this->replaceCountries = array_values($countryMap);
    }
}