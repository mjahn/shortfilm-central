<?php

namespace App\Jobs;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Storage;

class UploadFestivalMovie implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $retryAfter = 1800;

    /**
      * The number of times the job may be attempted.
      *
      * @var int
      */
     public $tries = 300;

    /**
     * Title of the movie
     * @var string
     */
    protected $source;

    /**
     * Target-path for the movie
     * @var string
     */
    protected $target;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($source, $target)
    {
        $this->source = $source;
        $this->target = $target;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        echo 'Starting upload of "' . basename($this->source) . '" [' . round(100 / 1024 * filesize($this->source) / 1024) / 100 . ' MB]' . "\n";

        Storage::disk('google')->put($this->target, fopen($this->source, 'r'));

        // add video-file to movie
        // Movie::where()
    }
}
