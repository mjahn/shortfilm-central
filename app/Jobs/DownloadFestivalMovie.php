<?php

namespace App\Jobs;

use App\Models\DriveVideo;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Storage;

class DownloadFestivalMovie implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public $retryAfter = 1800;

    /**
      * The number of times the job may be attempted.
      *
      * @var int
      */
     public $tries = 300;

    /**
     * Uri of the movie
     * @var string
     */
    protected $url;

    /**
     * Title of the movie
     * @var string
     */
    protected $title;

    /**
     * Year of the movie
     * @var int
     */
    protected $year;

    /**
     * Name of the director of the movie
     * @var string
     */
    protected $director;

    /**
     * Name of the festival
     * @var string
     */
    protected $festival;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url, $title, $year, $director = '', $festival = '')
    {
        $this->url = $url;
        $this->title = $title;
        $this->year = $year;
        $this->director = $director;
        $this->festival = $festival;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $target = \storage_path('app' . DIRECTORY_SEPARATOR. 'downloads' . DIRECTORY_SEPARATOR . $this->getFilename());

        // echo 'Checking if file already exists in Google Drive: "' . $this->getFilename() . '"' . "\n";
        if ($this->exists() && $this->filesize() > 10 * 1024 * 1024) {
            // echo 'Skipping already downloaded uri ' . $uri . "\n";
            return;
        }

        $url = parse_url($this->url);
        $baseUri = $url['scheme'].'://' . $url['host'];
        $path = $url['path'] . '?' . ($url[' query'] ?? '');
        $client = new Client([
            'base_uri' => $baseUri
        ]);
        try {
            echo $this->url ."\n";
            $response = $client->request('HEAD', $path);
            $contentType = $response->getHeader('Content-Type')[0];
            $contentLength = $response->getHeader('Content-Length')[0];

            if (disk_free_space(dirname($target)) < 5 * 1024 * 1024 * 1024) {
                throw new Exception('No more space in directory '  . dirname($target), 500);
            }

            // echo 'Starting download of "' . $this->getFilename() . '" [' . round(100 / 1024 * $contentLength / 1024) / 100 . ' MB]' . "\n";
            
            if ($contentType !== 'video/mp4') {
                // echo 'Festival Movie Uri has wrong content-type' . "\n";
                throw new Exception('Wrong contenttype for uri ' . $baseUri, 500);
            }
            $response = $client->request('GET', $path, [
                'sink' => $target
            ]);


        } catch (ClientException $exception) {
            throw new Exception('Could not find uri ' . $baseUri . $path, $exception->getCode(), $exception);
        }

        //dispatch(new UploadFestivalMovie($target, '/'.$this->getFilename()))->onQueue('upload');
    }

    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {
        Storage::disk('local')->delete('app' . DIRECTORY_SEPARATOR . 'downloads' . DIRECTORY_SEPARATOR . $this->getFilename());
    }

    protected function exists() 
    {
        return  DriveVideo::where('name', '=', $this->getFilename())->count() > 0 || Storage::disk('local')->exists('app' . DIRECTORY_SEPARATOR . 'downloads' . DIRECTORY_SEPARATOR . $this->getFilename());
    }

    protected function filesize()
    {
        $file = DriveVideo::where('name', '=', $this->getFilename())->first();
        if ($file) {
            return $file->size;
        }
        if (Storage::disk('local')->exists('app' . DIRECTORY_SEPARATOR . 'downloads' . DIRECTORY_SEPARATOR . $this->getFilename())) {
            return Storage::disk('local')->size('app' . DIRECTORY_SEPARATOR . 'downloads' . DIRECTORY_SEPARATOR . $this->getFilename());
        }
    }

    protected function getFilename() 
    {
        $replace = [
            '?' => '_', 
            '\\' => '_', 
            '/' => '_', 
            ':' => '_', 
            '*' => '_', 
            '<' => '_', 
            '>' => '_', 
            '|' => '_',
            '&#039;' => '\'',
            '&amp;' => '&',
        ];
        return html_entity_decode(str_replace(array_keys($replace), array_values($replace), $this->title)) . ' (' . $this->year . ', ' . html_entity_decode(str_replace(array_keys($replace), array_values($replace), $this->director)) .').mp4';
    }
}
