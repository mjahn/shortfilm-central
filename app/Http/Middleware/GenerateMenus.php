<?php

namespace App\Http\Middleware;

use App\Models\Category;
use App\Models\Movie;
use Closure;
use Spatie\Menu\Html;
use Spatie\Menu\Link;
use Menu;
use Auth;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::macro('main', function () {
            $user = Auth::user();

            $years = Movie::distinct('year')->orderBy('year', 'desc')->get('year')->pluck('year');
            $yearMenu = [];
            foreach ($years as $year) {
                $yearMenu[route('movies.index') . '?filter[year]=' . $year] = $year;   
            }

            $categories = Category::orderByTranslation('name', 'asc')->get([ 'name', 'id' ])->pluck('name', 'id');
            $categoryMenu = [];
            foreach ($categories as $id => $name) {
                $categoryMenu[route('movies.index') . '?filter[category]=' . rawurlencode($name)] = $name;   
            }

            return Menu::new()
                ->addClass('vertical medium-horizontal menu')
                ->setAttribute('data-responsive-menu', 'accordion medium-dropdown')
                ->setAttribute('id', 'main-menu')
                ->add(Html::raw('<li class="menu-text">' . config('app.name', 'Shortfilm Central') . '</li>'))
                ->add(Link::to(route('home'), '<span class="fi-home hide-for-small-only" title="' . __('navigation.home') . '"></span><span class="hide-for-medium">' . __('navigation.home') . '<span>'))
                ->submenu('<a href="' . route('movies.index') . '">'.__('navigation.movies') . '</a>', Menu::new()
                    ->addClass('vertical menu')
                    ->submenu('<a href="' . route('movies.index') . '">'.__('navigation.movies_by_year') . '</a>', Menu::build($yearMenu, function ($menu, $label, $link) {
                        $menu->addClass('vertical menu')->add(Link::to($link, $label));
                    }))->addClass('vertical menu')
                    ->submenu('<a href="' . route('movies.index') . '">'.__('navigation.movies_by_category') . '</a>', Menu::build($categoryMenu, function ($menu, $label, $link) {
                        $menu->addClass('vertical menu')->add(Link::to($link, $label));
                    })->addClass('vertical menu'))
                )
                ->add(Link::to(route('people.index'), __('navigation.people')))
                ->add(Link::to(route('events.index'), __('navigation.events')))
                ->submenuIf($user !== null && $user->isAdmin(), '<a href="/admin" target="blank">'.__('navigation.admin') . '</a>', Menu::new()
                    ->addClass('vertical menu')
                    ->add(Link::to('/translations', __('navigation.translations'))->setAttribute('target', 'blank'))
                    ->add(Link::to('/admin', __('navigation.administration'))->setAttribute('target', 'blank'))
                )
                ->setActiveFromRequest();
        });

         Menu::macro('user', function () {
            $user = Auth::user();
            $authenticated = $user !== null && !Auth::guest();

            $languageMenu = [];
            foreach (language()->allowed() as $code => $name) {
                $languageMenu[language()->back($code)] = '<img src="/images/flags/'. $code .'.png" alt="' . $name . '" class="' . config('language.flags.img_class') . '" width="' . config('language.flags.width') . '" />&nbsp;' . $name;
            }

            $menu = Menu::new()
                ->withoutWrapperTag()
                ->addClass('vertical medium-horizontal menu')
                ->setAttribute('data-responsive-menu', 'accordion medium-dropdown')
                ->setAttribute('id', 'user-menu');
            if (!$authenticated) {
                $menu->add(Link::to('/login', __('navigation.login')))
                    ->add(Link::to('/register', __('navigation.register')));

            }
            
            $menu->submenu('<a><img src="/images/flags/'. language()->getCode() .'.png" width="' . config('language.flags.width') . '" /></a>', Menu::build($languageMenu, function ($menu, $label, $link) {
                $menu->addClass('vertical menu nested')->add(Link::to($link, $label));
            }))->addClass('vertical menu');

            if ($authenticated) {
                $menu->submenu('<a href="' . Auth::user()->link()  .'" title="' . Auth::user()->name . '"><span class="fi-torso"></span></a>', Menu::new()
                    ->addClass('vertical menu')
                    ->add(Link::to($user->sublink('profile'), __('navigation.userprofile')))
                    ->add(Link::to(route('collections.index'), __('navigation.collections')))
                    ->add(Html::raw('<a href="/logout" title="' . __('navigation.logout') . '" onclick="event.preventDefault();document.getElementById(\'logout-form\').submit();">'.__('navigation.logout').'</a>'))
                );
            }
            return $menu;
        });
        return $next($request);
    }
}
