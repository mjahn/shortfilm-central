<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventTranslation;
use App\Models\Category;
use App\Models\DriveVideo;
use App\Models\Movie;
use App\Models\MovieTranslation;
use App\Models\Person;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use Spatie\Searchable\ModelSearchAspect;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the search-result
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $searchterm = $request->input('search');
        $locale = \App::getLocale();

        $people = Person::search($searchterm)->get();
        $events = Event::whereIn('id', EventTranslation::search($searchterm)->where('locale', $locale)->get([ 'event_id' ])->pluck('event_id'))->get();
        $movies = Movie::whereIn('id', MovieTranslation::search($searchterm)->where('locale', $locale)->get([ 'movie_id' ])->pluck('movie_id'))->get();
        $videos = DriveVideo::search($searchterm)->get();

        $count = $people->count() + $events->count() + $movies->count() + $videos->count();
        return view('search', ['searchResults' => compact('people', 'events', 'movies', 'videos', 'count'), 'searchterm' => $searchterm ]);
    }

    /**
     * Show the search-result
     *
     * @return \Illuminate\Http\Response
     */
    public function autocomplete(Request $request)
    {
        $searchterm = $request->input('term');
        $locale = \App::getLocale();

        $results = collect([]);
        $results = $results->concat(Person::where('name', 'like', '%' . $searchterm . '%')->get([ 'name' ])->pluck('name'));
        $results = $results->concat(EventTranslation::where('name', 'like', '%' . $searchterm . '%')->where('locale', $locale)->get([ 'name' ])->pluck('name'));
        $results = $results->concat(MovieTranslation::where('name', 'like', '%' . $searchterm . '%')->where('locale', $locale)->get([ 'name' ])->pluck('name'));
        $results = $results->concat(DriveVideo::where('name', 'like', '%' . $searchterm . '%')->get([ 'name' ])->pluck('name'));
        return $results->values()->unique()->sort()->values()->all();
    }
}
