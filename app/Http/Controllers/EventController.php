<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Movie;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $events = Event::whereHas('movies')->orderBy('start_at', 'desc')->paginate(20);
        return view('event.list', ['events' => $events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Event $event)
    {
        $event->load([ 'programs', 'programs.event' ]);
        $search = trim((string) $request->input('search'));

        $movieQuery = Movie::where(function ($query) use ($event) {
            $query->whereHas('eventPrograms', function ($query) use ($event) {
                $query->where('event_programs.event_id', '=', $event->id);
            })
            ->orWhereHas('events', function ($query) use ($event) {
                $query->where('events.id', '=', $event->id);
            });
        });

        if ($search !== '') {
            $movieQuery = $movieQuery->search($search);
        }
        
        $movies = $movieQuery->with([ 'events', 'media', 'countries' ])->orderByTranslation('name', 'asc')->paginate(20);
        return view('event.show', compact('event', 'movies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
}
