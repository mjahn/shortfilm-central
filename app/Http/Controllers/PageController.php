<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the pages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Page::where('slug', '=', 'home')->first();
        return view('page.show', compact('page'));
    }

    /**
     * Display the specified page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Tcg\Voyager\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Page $page)
    {
        return view('page.show', compact('page'));
    }
}
