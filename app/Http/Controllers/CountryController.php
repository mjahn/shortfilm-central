<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = trim((string) $request->input('search'));

        if ($search !== '') {
            $countries = Country::search($search)->orderBy('start_at')->paginate(20);
        } else {
            $countries = Country::orderBy('start_at')->paginate(20);
        }
        return view('country.list', ['countries' => $countries]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Country $country)
    {
        $movies = $country->movies()->with([ 'events', 'media', 'countries' ])->orderByTranslation('name', 'asc')->paginate(20);
        return view('country.show', compact('country', 'movies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\countrie  $countrie
     * @return \Illuminate\Http\Response
     */
    public function edit(countrie $countrie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\countrie  $countrie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, countrie $countrie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\countrie  $countrie
     * @return \Illuminate\Http\Response
     */
    public function destroy(countrie $countrie)
    {
        //
    }
}
