<?php

namespace App\Http\Controllers;

use App\Models\Test;
use Illuminate\Http\Request;

class EncryptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //
        $tests = Test::all();
        return response()->json($tests);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        //
        $test = Test::find($id);
        return response()->json($test);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $data)
    {
        //
        //
        $test = new Test($data);
        $test->save();
        return response();
    }
}
