<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\EventProgram;
use Illuminate\Http\Request;

class EventProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Event $event)
    {
        $programs = $event->programs()->with('event')->orderBy('name', 'asc')->get();
        return view('program.list', compact(['programs', 'event']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventProgram  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Event $event, EventProgram $program)
    {
        $search = trim((string) $request->input('search'));

        if ($search !== '') {
            $movies = $program->movies()->search($search)->with([ 'events', 'media' ])->orderByTranslation('name', 'asc')->paginate(20);
        } else {
            $movies = $program->movies()->orderByTranslation('name', 'asc')->with([ 'events', 'media' ])->paginate(20);
        }
        return view('program.show', compact(['program', 'movies', 'event']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventProgram  $eventProgram
     * @return \Illuminate\Http\Response
     */
    public function edit(EventProgram $eventProgram)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventProgram  $eventProgram
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventProgram $eventProgram)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventProgram  $eventProgram
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventProgram $eventProgram)
    {
        //
    }
}
