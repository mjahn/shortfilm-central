<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use App\Repositories\MovieRepository;
use EloquentBuilder;
use Illuminate\Http\Request;
use Webpatser\Countries\Countries;

class MovieController extends Controller
{
    /**
     * Instance of the movie repository
     * @var \App\Repositories\MovieRepository
     */
    private $movieRepository;

    public function __construct(MovieRepository $movieRepository)
    {
        $this->movieRepository = $movieRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        if ($request->has('filter')) {
            $filter = $request->only('filter')['filter'];
        }   
        $movies = EloquentBuilder::to(Movie::class, $filter)->orderByTranslation('name', 'asc')->with([ 'translations', 'countries', 'media' ])->paginate(20);
        if (count($filter) > 0) {
            $movies = $movies->appends([ 'filter' => $filter ]);
        }
        return view('movie.list', compact('movies', 'filter'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movie.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        $movie->load(['casts.person', 'casts.castType', 'countries', 'externals', 'categories']);
        $events = $movie->eventPrograms()->get();
        $events = $events->filter(function ($item) {
            return $item->name !== '';
        });
        $events = $events->groupByModel('event');
        return view('movie.show', compact('movie', 'events'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        //
    }
}
