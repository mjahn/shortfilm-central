<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $movies = Movie::orderByRaw('RAND()')->take(10)->get();
 
        return view('dashboard.list', ['movies' => $movies]);
    }
}
