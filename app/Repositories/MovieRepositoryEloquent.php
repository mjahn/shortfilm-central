<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MovieRepository;
use App\Models\Movie;
use App\Validators\MovieValidator;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Cast;
use App\Models\Person;

/**
 * Class MovieRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MovieRepositoryEloquent extends BaseRepository implements MovieRepository {

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'year',
        'description' => 'like',
        'director.name' => 'like',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Movie::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Get a movie by its name, year and the name of the director(s)
     * 
     * @param  string $name         name of the movie
     * @param  int $year            year of the movie
     * @param  string $director     name of the director of the movie
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByNameYearAndDirector($name, $year, $director): Collection
    {
        $casts = Cast::whereHas('person', function ($query) use ($director) {
            $query->where('name', '=', $director);
        })->whereHas('castType', function ($query) {
            $query->where('name', '=', 'director');
        })->get();

        return Movie::where([
            [ 'movies.name', '=', $name ],
            [ 'movies.year', '=', $year ],
        ])->whereHas('casts', function($query) use ($casts) {
            $query->whereIn('id', $casts->pluck('id'));
        })->with('casts')->get();
    }
}
