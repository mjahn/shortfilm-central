<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Interface MovieRepository.
 *
 * @package namespace App\Repositories;
 */
interface MovieRepository extends RepositoryInterface
{
    /**
     * Get a movie by its name, year and the name of the director(s)
     * 
     * @param  string $name         name of the movie
     * @param  int $year            year of the movie
     * @param  string $director     name of the director of the movie
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function findByNameYearAndDirector($name, $year, $director): Collection;
}
