<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PersonRepository;
use App\Models\Person;
use App\Validators\PersonValidator;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class PersonRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class PersonRepositoryEloquent extends BaseRepository implements PersonRepository
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like',
        'year_of_birth',
        'bio' => 'like',
        'notes' => 'like',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Person::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Get a person by its name and country
     * 
     * @param  string $name         name of the person
     * @param  string $countryCode  country of the person
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getByNameAndCountry($name, $countryCode)
    {
        return Person::where('name', 'like', $name)->get();
    }
    
}
