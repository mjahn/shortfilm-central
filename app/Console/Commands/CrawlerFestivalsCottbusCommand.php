<?php

namespace App\Console\Commands;

use App\Jobs\ImportMovieJob;
use App\Models\ImportMovie;
use App\Services\HttpCrawlerService;
use Illuminate\Console\Command;
use Log;

class CrawlerFestivalsCottbusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:festivals:cottbus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawls the website of the Cottbus Film Festival and grab information about the festival and the shortfilms';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Crawling the Cottbus Film Festival movie database page');
        $config = config('crawler.festivals.cottbus', []);
        $crawler = new HttpCrawlerService($config['host']);
        if (isset($config['events'])) {
            $this->info('Fetching events list');

            $eventsResult = $crawler->crawl(
                $config['events']['urls'], 
                $config['events']['steps']
            );
        }

        foreach ($eventsResult['ids'] as $index => $id) {
            $eventYear = $eventsResult['years'][$index];
            $eventName = (29 + $eventYear - 2019) . '. FilmFestival Cottbus';

            // fetch movie-list of this event
            $eventResult = $crawler->crawl(
                [ $config['event']['url_pattern']. $id ], 
                $config['event']['steps']
            );

            // fetch movies of this event

            $urls = [];
            foreach ($eventResult['movies'] as $data) {
                $data = explode('"', $data);
                $urls[] = '/de/festival/archiv/movie/' . $data[0];
            }

            $this->info('Fetching movies');
            foreach ($urls as $url) {
                $result = $crawler->crawl(
                    [ $url ], 
                    $config['movie']['steps']
                );

                // print_r($result);
                $movieData['event'] = $eventName;
                $movieData['programs'] = [ optional($result['program'])[0] ];
                $movieData['title'] = $result['title'][0];
                $movieData['title_german'] = $result['title_german'][0];
                $movieData['length'] = 60 * $result['length'][0];
                $movieData['year'] = intval($result['year'][0], 10);
                $movieData['countries'] = array_map(function ($text) { 
                        return $this->cleanupString($text);
                    }, explode(',', $result['country'][0]));;
                $movieData['length'] = 60 * $result['length'][0];
                if (isset($result['image']) && count($result['image']) > 0) {
                    $movieData['image'] = $config['host'] . $result['image'][0];
                }
                $movieData['description'] = '';
                if (optional($result['teaser'])[0]) {
                    $movieData['description'] .= $this->cleanupString($result['teaser'][0]) . "\n\n";
                }
                $movieData['description'] .= $this->cleanupString($result['description'][0]);
                
                $movieData['casts'] = [
                    'director' => $result['director']
                ];
                if (optional($result['director_description'])[0]) {
                    $movieData['director_description'] = $result['director_description'][0];
                }
                if (optional($result['director_image'])[0]) {
                    $movieData['director_image'] = $config['host'] . $result['director_image'][0];
                }
                if (optional($result['trailer'])[0]) {
                    $movieData['trailer'] = $result['trailer'];
                }

                $movieData['externals'] = [
                    $config['host'] . $url
                ];
                $movieData['other'] = [];
    
                $rolesMap = config('crawler.festivals.cottbus.maps.roles', []);
                foreach ($result['key'] as $index => $key) {
                    if (isset($rolesMap[$key])) {
                        $movieData['casts'][$rolesMap[$key]] = array_map(function ($text) { 
                            return $this->cleanupString($text);
                        }, explode(',', $result['value'][$index]));;
                        continue;
                    }

                    $movieData['other'][$key] = array_map(function ($text) { 
                        return $this->cleanupString($text);
                    }, explode(',', $result['value'][$index]));;
                }

                dispatch(new ImportMovieJob(new ImportMovie($movieData)))->onQueue('import');
            }
        }
    }

    protected function cleanupString($text)
    {
        $text = html_entity_decode($text);
        $text = strip_tags($text);
        $text = trim($text);
        return $text;
    }

    protected function parseTimespan($text) {
        $timespan = trim(explode('min', $text)[0]);
        return $timespan * 60;
    }
}
