<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Spatie\MediaLibrary\Jobs\GenerateResponsiveImages;
use Spatie\MediaLibrary\Models\Media;

class RegenerateResponsiveImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'medialibrary:regenerate-responsive-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Media::chunk(500, function ($medias) {
            foreach ($medias as $media) dispatch(new GenerateResponsiveImages($media))->onQueue('images');
        });
    }
}
