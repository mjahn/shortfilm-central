<?php

namespace App\Console\Commands;

use App\Models\External;
use App\Models\ExternalType;
use App\Models\Media;
use App\Models\Movie;
use App\Models\Person;
use App\Models\MovieTranslation;
use App\Services\HttpCrawlerService;
use Illuminate\Console\Command;
use Log;

class CrawlerWebsiteTestkammerCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:websites:testkammer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawls the website of testkammer.com and grab information about the festival and the shortfilms';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Crawling page testkammer.com');
        $config = config('crawler.websites.testkammer', []);

        $crawler = new HttpCrawlerService($config['host']);

        $reviewType = ExternalType::whereName('review')->first()->id;
        $linkType = ExternalType::whereName('link')->first()->id;
        $interviewType = ExternalType::whereName('interview')->first()->id;


        if (isset($config['reviews'])) {
            $this->info(' + crawling review index');
            $result = $crawler->crawl(
                $config['reviews']['urls'],
                $config['reviews']['steps']
            );
            $urls = [];
            for ($index = 0; $index < count($result['url']); $index++) {
                $url = $result['url'][$index];
                $title = htmlspecialchars($result['title'][$index]);
                $year = $result['year'][$index];
                $urls[] = $url;

                $movies = MovieTranslation::whereHas('movie', function ($query) use ($year) {
                    $query->where('year', '=', $year);
                })->whereRaw('lower(name) = ?', [ strtolower($title) ])->with('movie')->get();
                if ($movies->count() === 0) {
                    continue;
                }

                foreach ($movies as $movieTranslation) {
                    $movie_id = $movieTranslation->movie_id;
                    External::firstOrCreate([
                        'value' => $url,
                        'externable_id' => $movie_id,
                        'externable_type' => Movie::class,
                        'name' => 'Rezension auf testkammer.com',
                        'external_type_id' => $reviewType,
                    ]);
                }
            }
        }

        // parse pages and extract "Quellen" as additional externals

        if (isset($config['review'])) {
            $this->info(' + crawling review pages');
            foreach ($urls as $url) {
                $result = $crawler->crawl(
                    [ $url ],
                    $config['review']['steps']
                );

                // get the movie for the current url
                $external = External::whereValue($url)->whereExternableType(Movie::class)->whereExternalTypeId($reviewType)->first();
                if (is_null($external)) {
                    continue;
                }
                $movie_id = $external->externable_id;
                for ($index = 0; $index < count($result['links']); $index++) {
                    $link = $result['links'][$index];
                    $title = htmlspecialchars(str_replace('&amp;nbsp;', ' ', htmlspecialchars($result['titles'][$index])));

                    if (strpos($link, '://testkammer.com/') !== false) {
                        continue;
                    }

                    External::firstOrCreate([
                        'value' => $link,
                        'externable_id' => $movie_id,
                        'externable_type' => Movie::class,
                        'external_type_id' => $linkType,
                    ], [
                        'name' => $title,
                    ]);
                }
            }
        }

        if (isset($config['interviews'])) {
            $this->info(' + crawling interview index');
            $result = $crawler->crawl(
                $config['interviews']['urls'],
                $config['interviews']['steps']
            );

            $urls = [];
            for ($index = 0; $index < count($result['url']); $index++) {
                $url = $result['url'][$index];
                $urls[] = str_replace($config['host'], '', $url);
                $title = htmlspecialchars($result['title'][$index]);

                $people = Person::whereRaw('lower(name) = ?', [ strtolower($title) ])->get();
                if ($people->count() === 0) {
                    continue;
                }
                $interviewType = ExternalType::whereName('interview')->first()->id;
                foreach ($people as $person) {
                    External::firstOrCreate([
                        'value' => $url,
                        'externable_id' => $person->id,
                        'externable_type' => Person::class,
                        'external_type_id' => $interviewType,
                    ], [
                        'name' => 'Interview auf testkammer.com',
                    ]);
                }
            }
        }

        if (isset($config['interview'])) {
            $this->info(' + crawling interview page for person image');
            foreach ($urls as $url) {
                $result = $crawler->crawl(
                    [ $url ],
                    $config['interview']['steps']
                );

                $filename = htmlspecialchars($result['title'][0]);
                $label = htmlspecialchars(str_replace('&amp;nbsp;', ' ', htmlspecialchars($result['name'][0])));
                $image = htmlspecialchars($result['image'][0]);

                $external = External::whereValue($config['host'] . $url)->whereExternalTypeId($interviewType)->first();
                if (is_null($external)) {
                    continue;
                }
                $person = $external->externable()->first();

                if (Media::whereModelId($person->id)->whereModelType('App\Models\Person')->whereName($label)->whereFileName($filename)->exists()) {
                    continue;
                }

                $person                
                    ->addMediaFromUrl($image)
                    ->usingFileName($filename)
                    ->usingName($label)
                    ->withResponsiveImages()
                    ->toMediaCollection('images');
            }
        }
    }
}
