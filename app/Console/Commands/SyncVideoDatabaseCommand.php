<?php

namespace App\Console\Commands;

use App\Services\DriveVideoService;
use Illuminate\Console\Command;

class SyncVideoDatabaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:drive:videos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync the files in the Google Drive video-database with the local database-table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DriveVideoService::syncToDatabase();
    }
}
