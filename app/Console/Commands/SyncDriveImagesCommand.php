<?php

namespace App\Console\Commands;

use App\Services\DriveImagesService;
use Illuminate\Console\Command;

class SyncDriveImagesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:drive:images {slug?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync the files in the Google Drive with the local medialibrary';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DriveImagesService::syncToMediaLibrary($this->argument('slug'));
    }
}
