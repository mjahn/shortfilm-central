<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\HttpCrawlerService;
use App\Jobs\ImportMovieJob;
use App\Models\ImportMovie;


class CrawlerFestivalsLandshutCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:festivals:landshut';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawls the website of the Landshut Shortfilm Festival and grab information about the festival and the shortfilms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Crawling Landshut Festival page');
        $config = config('crawler.festivals.landshut', []);

        $crawler = new HttpCrawlerService($config['host']);
        if (!isset($config['festival'])) {
            $this->error('Failed loading config');
            return;
        }
        $this->info('Fetching movie-list');

        $result = $crawler->crawl(
            $config['festival']['urls'], 
            $config['festival']['steps']
        );

        foreach ($result['movie_urls'] as $url) {
            $result = $crawler->crawl(
                [ $url ], 
                $config['movie']['steps']
            );

            $movieData['title'] = $result['title'][0];
            $movieData['description'] = $result['description'][0];
            $movieData['programs'] = explode(',', $result['program'][0]);
            $movieData['countries'] = explode(',', $result['country'][0]);
            $movieData['year'] = $result['year'][0];

            $movieData['category'] = [];
            if (count($result['category']) > 0) {
                $movieData['category'] = explode(',', $result['category'][0]);
            }
            $movieData['length'] = $this->parseTimespan($result['length'][0]);
            $movieData['original_language'] = $result['language'][0];

            $movieData['image'] = [];
            if (count($result['image']) > 0) {
                $movieData['image'] = $result['image']; 
            }

            $movieData['poster'] = [];
            if (count($result['poster']) > 0) {
               $movieData['poster'] = $result['poster']; 
            }

            $movieData['trailer'] = [];
            if (count($result['trailer']) > 0) {
               $movieData['trailer'] = $result['trailer']; 
            }

            $movieData['event'] = '20. Landshuter Kurzfilmfestival';
            $movieData['casts'] = [];
            foreach (config('crawler.festivals.landshut.maps.roles', []) as $key => $value) {
                for ($index = 0; $index < count($result['roles']); $index++) {
                    if ($result['roles'][$index] != $key) {
                        continue;
                    }
                    $movieData['casts'][$value] = $result['names'][$index];
                }
            }
            $movieData['contact'] = [];
            foreach (config('crawler.festivals.landshut.maps.contact', []) as $key => $value) {
                for ($index = 0; $index < count($result['roles']); $index++) {
                    if ($result['roles'][$index] != $key) {
                        continue;
                    }
                    $movieData['contact'][$value] = $result['names'][$index];
                }
            }
           dispatch(new ImportMovieJob(new ImportMovie($movieData)))->onQueue('import');
        } 
    }

    protected function parseTimespan($text) {
        $text = str_replace('.', ':', $text);
        $text = explode(':', $text);
        $factor = 1;
        $timespan = 0;
        while (null !== ($part = array_pop($text))) {
            $timespan = $factor * $part;
            $factor *= 60;
        }
        return $timespan;
    }
}
