<?php

namespace App\Console\Commands;

use App\Jobs\ImportMovieJob;
use App\Models\ImportMovie;
use Illuminate\Console\Command;
use Log;

class ImportDatabaseDresdenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:database:dresden {source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import database of dresden filmfest';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Crawling Filmfest Dresden Database page');
        $file = $this->argument('source');
        $entries = file($file);
        foreach ($entries as $entry) {
            $entry = json_decode($entry, true);

            $movieData['title'] = $entry['title'];
            $movieData['description'] = $entry['description'];
            $movieData['programs'] = explode(',', $entry['program']);
            $movieData['countries'] = explode(',', $entry['country']);
            $movieData['year'] = $entry['year'];
            $movieData['category'] = explode(',', $entry['category']);
            $movieData['length'] = $this->parseTimespan($entry['length']);
            $movieData['original_language'] = $entry['language'];
            $movieData['premiere'] = $entry['premiere'];
            $movieData['casts'] = [
                'director' => $entry['regie'],
            ];
            $movieData['video'] = $entry['url']; 
            $movieData['event'] = '29. Internationales Kurzfilm Festival Dresden';
            dispatch(new ImportMovieJob(new ImportMovie($movieData)))->onQueue('import');
        }
    }

    protected function parseTimespan($text) {
        $text = explode(' min', $text)[0];
        $text = explode(':', $text);
        $factor = 1;
        $timespan = 0;
        while (null !== ($part = array_pop($text))) {
            $timespan = $factor * $part;
            $factor *= 60;
        }
        return $timespan;
    }
}
