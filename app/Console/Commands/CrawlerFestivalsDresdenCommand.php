<?php

namespace App\Console\Commands;

use App\Jobs\ImportMovieJob;
use App\Models\ImportMovie;
use App\Services\HttpCrawlerService;
use Illuminate\Console\Command;
use Log;

class CrawlerFestivalsDresdenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:festivals:dresden';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawls the website of the Dresden International Shortfilm Festival and grab information about the festival and the shortfilms';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Crawling Filmfest Dresden Database page');
        $config = config('crawler.festivals.dresden', []);

        $crawler = new HttpCrawlerService($config['host'], $config['before']);

        if (isset($config['program'])) {
            $urlPattern = $config['program']['url_pattern'];
            for($index = 0; $index <= 94; $index++) {
                $config['program']['urls'][] = $urlPattern . $index;
            }
            $this->info('Fetching program');
            $result = $crawler->crawl(
                $config['program']['urls'], 
                $config['program']['steps']
            );
        }

        // loop through all movies 
        // - create data-structure 
        // - create url for details
        // -
        $movies = []; 
        $urls = [];
        $this->info('Fetching movie-data');

        foreach ($result['movies'] as $content) {
            $movieData = [];

            // get id
            if (preg_match('/data-target="film-library-details-([\d]+)/', $content, $matches)) {
                $movieData['id'] = $matches[1];
            }

            // image
            if (preg_match('/<img src="([^"]+)"/', $content, $matches)) {
                $movieData['image'] = $matches[1];
            }
            // title
            if (preg_match('/ views-field-title"><span class="field-content">([^<]+)<\/span>/', $content, $matches)) {
                $movieData['title'] = $matches[1];
            }
            // english title
            if (preg_match('/ views-field-field-title-eng"><span class="field-content">\(([^<]+)\)<\/span>/', $content, $matches)) {
                $movieData['title_english'] = $matches[1];
            }
            // original language
            if (preg_match('/ views-field-field-original-language"><span class="field-content">\(([^<]+)\)<\/span>/', $content, $matches)) {
                $movieData['original_language'] = $matches[1];
            }
            // premiere status
            if (preg_match('/ views-field-field-premiere-status"><span class="field-content">\(([^<]+)\)<\/span>/', $content, $matches)) {
                $movieData['premiere'] = $matches[1];
            }
            // programs
            if (preg_match('/ views-field-field-programme"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movieData['programs'] = $matches[1];
            }
            // countries
            if (preg_match('/views-field-field-prod-country"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movieData['countries'] = array_map(function ($text) { 
                    return trim($text);
                }, explode(',', $matches[1]));
            }
            // year
            if (preg_match('/views-field-field-prod-year"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movieData['year'] = $matches[1];
            }
            // categories
            if (preg_match('/views-field-field-category"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movieData['category'] = $matches[1];
            }
            // length
            if (preg_match('/ views-field-nothing"><span class="field-content">(\d*\:\d+) min<\/span>/', $content, $matches)) {
                $movieData['length'] = $this->parseTimespan($matches[1]);
            }
            if (preg_match('/views-label-field-staff-regie">Regie: <\/span><div class="field-content">([^<]+)<\/div>/', $content, $matches)) {
                $movieData['director'] = $matches[1];
            }
            if (preg_match('/views-field-php"><span class="field-content">((?:<br\s*\/{0,1}>|[^<]+)+)</', $content, $matches)) {
                $movieData['description'] = $matches[1];
            }

            $movies[$movieData['id']] = $movieData;
            $urls[] = $config['movies_extended']['url_pattern'] . $movieData['id'];
        }

        if (isset($config['movies_extended'])) {
            $this->info('Fetching extended movie-data');

            $result = $crawler->crawl(
                $urls, 
                $config['movies_extended']['steps']
            );

            foreach ($result['movies'] as $content) {
                // get movie id
                if (!preg_match('/film-detail-(\d+)/', $content, $matches)) {
                    Log::error('Cannot fetch id from extended movie ' . $id, [ 'content' => $content]);
                    continue;
                }

                $id = $matches[1];
                $this->info('Parse extended moviedata for id ' . $id);
                $movieData = $movies[$id];

                if (preg_match('/src="([^"]+)"\s*\/>\s*<\/video>/', $content, $matches)) {
                    $movieData['video'] = $matches[1];
                }

                if (preg_match('/views-label-php">Distribution: <\/span><span class="field-content">([^<]+)/', $content, $matches)) {
                    $movieData['distribution'] = $matches[1];
                }

                if (preg_match('/href="mailto:([^"]+)/', $content, $matches)) {
                    $movieData['email'] = $matches[1];
                }

                preg_match_all('/views-label-field-staff-([^"]+)">[^<]+<\/span><div class="field-content">([^<]+)/', $content, $matches);
                for ($index = 0; $index < count($matches[0]); $index++) {
                    $movieData[$matches[1][$index]] = array_map(function ($text) { 
                        return trim($text);
                    }, explode(',', $matches[2][$index]));
                }
                $movieData['casts'] = [];
                foreach (config('crawler.festivals.dresden.maps.roles', []) as $key => $value) {
                    if (isset($movieData[$key])) {
                        $movieData['casts'][$value] = $movieData[$key];
                        unset($movieData[$key]);
                    }
                }
                $movieData['event'] = '31. Internationales Kurzfilm Festival Dresden';

                dispatch(new ImportMovieJob(new ImportMovie($movieData)))->onQueue('import');
            }
        }
    }

    protected function parseTimespan($text) {
        $text = explode(':', $text);
        $factor = 1;
        $timespan = 0;
        while (null !== ($part = array_pop($text))) {
            $timespan = $factor * $part;
            $factor *= 60;
        }
        return $timespan;
    }
}
