<?php

namespace App\Console\Commands;

use App\Jobs\ImportMovieJob;
use App\Models\ImportMovie;
use App\Services\HttpCrawlerService;
use Illuminate\Console\Command;
use Log;

class CrawlerFestivalsDokLeipzigCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:festivals:dok_leipzig';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawls the website of the DOK Leipzigl and grab information about the festival and the shortfilms';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Crawling the DOK Leipzig movie database page');
        $config = config('crawler.festivals.dok_leipzig', []);

        $crawler = new HttpCrawlerService($config['host']);
        if (isset($config['program-list'])) {
            $this->info('Fetching program list');

            $result = $crawler->crawl(
                $config['program-list']['urls'], 
                $config['program-list']['steps']
            );
        }

        if (!isset($config['program'])) {
            return;
        }

        $urlPattern = $config['program']['url_pattern'];
        foreach ($result['id'] as $id) {
            $config['program']['urls'][] = $urlPattern . $id;
        }
        $this->info('Fetching programs');
        $result = $crawler->crawl(
            $config['program']['urls'], 
            $config['program']['steps']
        );

        $urls = [];
        foreach ($result['time'] as $index => $duration) {
            // if ($duration > $config['max_length']) {
            //     continue;
            // }
            $urls[] = $config['movie']['url_pattern'] . $result['ids'][$index];
        }

        $this->info('Fetching movies');
        foreach ($urls as $url) {
            $result = $crawler->crawl(
                [ $url ], 
                $config['movie']['steps']
            );

            $movieData['title_english'] = $result['title'][0];
            $movieData['event'] = $this->parseEvent($result['event'][0]);
            $movieData['programs'] = [ $result['program'][0] ];
            if (isset($result['image']) && count($result['image']) > 0) {
                $movieData['image'] = $config['host'] . $result['image'][0];
            }
            $movieData['description'] = trim($result['description'][0]);
            $movieData['casts'] = [];
            $movieData['other'] = [];
            $rolesMap = config('crawler.festivals.dok_leipzig.maps.roles', []);
            foreach ($result['key'] as $index => $key) {
                if ($key === 'Originaltitel') {
                    $movieData['title'] = $result['value'][$index];
                    continue;
                }
                if ($key === 'Land') {
                    $movieData['countries'] = array_map(function ($text) { 
                        return trim($text);
                    }, explode(',', $result['value'][$index]));;
                    continue;
                }
                if ($key === 'Originaltitel') {
                    $movieData['title'] = $result['value'][$index];
                    continue;
                }
                if ($key === 'Originaltitel') {
                    $movieData['title'] = $result['value'][$index];
                    continue;
                }
                if ($key === 'Jahr') {
                    $movieData['year'] = $result['value'][$index];
                    continue;
                }
                if ($key === 'Laufzeit') {
                    $movieData['length'] = $this->parseTimespan($result['value'][$index]);
                    continue;
                }
                if (isset($rolesMap[$key])) {
                    $movieData['casts'][$rolesMap[$key]] = array_map(function ($text) { 
                        return trim($text);
                    }, explode(',', $result['value'][$index]));;
                    continue;
                }
                $movieData['other'][$key] = array_map(function ($text) { 
                    return trim($text);
                }, explode(',', $result['value'][$index]));;
            }

            $data = explode('|', $result['meta'][0]);
            if (count($data) > 3) {
                $movieData['category'] = trim($data[2]);
            }

            if (!isset($movieData['title'])) {
                $movieData['title'] = $movieData['title_english'];
            }
            // print_r($movieData);
            // return;

            dispatch(new ImportMovieJob(new ImportMovie($movieData)))->onQueue('import');
        }
    }

    protected function parseTimespan($text) {
        $timespan = trim(explode('min', $text)[0]);
        return $timespan * 60;
    }

    protected function parseEvent($text) {
        $data = explode(' ', $text);
        $year = array_shift($data);
        $number = 62 - 2019 + $year;
        return $number . '. Internationales Leipziger Festival für Dokumentar- und Animationsfilm';
    }
}
