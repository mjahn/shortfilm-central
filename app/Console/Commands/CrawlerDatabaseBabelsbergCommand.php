<?php

namespace App\Console\Commands;

use App\Jobs\DownloadFestivalMovie;
use App\Services\HttpCrawlerService;
use Illuminate\Console\Command;

class CrawlerDatabaseBabelsbergCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawler:database:babelsberg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawls the film database of Filmuniversität Babelsberg Konrad Wolf';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Crawling Filmuniversität Babelsberg Konrad Wolf Database page');
        $config = config('crawler.database.babelsberg', []);

        $crawler = new HttpCrawlerService($config['host'], $config['before']);
        // if (isset($config['program-list'])) {
        //     $this->info('Fetching program list');

        //     $result = $crawler->crawl(
        //         $config['program-list']['urls'], 
        //         $config['program-list']['steps']
        //     );
        // }

        if (isset($config['index'])) {
            $this->info('Fetching index');
            $result = $crawler->crawl(
                $config['index']['urls'], 
                $config['index']['steps']
            );
        }

        // loop through all movies 
        // - create data-structure 
        // - create url for details
        // -
        $movies = []; 
        $urls = [];
        foreach ($result['movies'] as $content) {
            $movie = [];

            // get id
            if (preg_match('/data-target="film-library-details-([\d]+)/', $content, $matches)) {
                $movie['id'] = $matches[1];
            }

            // get title
            if (preg_match('/<img src="([^"]+)"/', $content, $matches)) {
                $movie['image'] = $matches[1];
            }
            if (preg_match('/ views-field-title"><span class="field-content">([^<]+)<\/span>/', $content, $matches)) {
                $movie['title'] = $matches[1];
            }
            if (preg_match('/ views-field-field-title-eng"><span class="field-content">\(([^<]+)\)<\/span>/', $content, $matches)) {
                $movie['title_english'] = $matches[1];
            }
            if (preg_match('/ views-field-field-programme"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movie['programs'] = $matches[1];
            }
            if (preg_match('/views-field-field-prod-country"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movie['countries'] = array_map(function ($text) { 
                    return trim($text);
                }, explode(',', $matches[1]));
            }
            if (preg_match('/views-field-field-prod-year"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movie['year'] = $matches[1];
            }
            if (preg_match('/views-field-field-category"><div class="field-content">([^<]+)</', $content, $matches)) {
                $movie['category'] = $matches[1];
            }
            if (preg_match('/ views-field-nothing"><span class="field-content">(\d*\:\d+) min<\/span>/', $content, $matches)) {
                $movie['length'] = $this->parseTimespan($matches[1]);
            }
            if (preg_match('/views-label-field-staff-regie">Regie: <\/span><div class="field-content">([^<]+)<\/div>/', $content, $matches)) {
                $movie['director'] = $matches[1];
            }
            if (preg_match('/views-field-php"><span class="field-content">((?:<br\s*\/{0,1}>|[^<]+)+)</', $content, $matches)) {
                $movie['description'] = $matches[1];
            }

            $movies[$movie['id']] = $movie;
            $urls[] = $movie['id'];
        }

        if (isset($config['movies_extended'])) {
            $urlPattern = $config['movies_extended']['url_pattern'];
            foreach ($movies as $id => $movie) {
                $config['movies_extended']['urls'][] = $urlPattern . $id;
            }
            $this->info('Fetching extended movies data');
            $result = $crawler->crawl(
                $config['movies_extended']['urls'], 
                $config['movies_extended']['steps']
            );
        }

        foreach ($result['movies'] as $content) {
            $movie = [];

            // get title
            if (preg_match('/film-detail-(\d+)/', $content, $matches)) {
                $id = $matches[1];
            }
            $movie = $movies[$id];

            if (preg_match('/src="([^"]+)"\s*\/>\s*<\/video>/', $content, $matches)) {
                $movie['video'] = $matches[1];
            }

            if (preg_match('/views-label-php">Distribution: <\/span><span class="field-content">([^<]+)/', $content, $matches)) {
                $movie['distribution'] = $matches[1];
            }

            if (preg_match('/href="mailto:([^"]+)/', $content, $matches)) {
                $movie['email'] = $matches[1];
            }

            preg_match_all('/views-label-field-staff-([^"]+)">[^<]+<\/span><div class="field-content">([^<]+)/', $content, $matches);
            for ($index = 0; $index < count($matches[0]); $index++) {
                $movie[$matches[1][$index]] = array_map(function ($text) { 
                    return trim($text);
                }, explode(',', $matches[2][$index]));
            }

            $movies[$movie['id']] = $movie;
            
            dispatch(new DownloadFestivalMovie($movie['video'], $movie['title'], $movie['year'], $movie['director'], '31. Internationales Kurzfilm Festival Dresden 2019'))->onQueue('download');
        }
    }

    protected function parseTimespan($text) {
        $text = explode(':', $text);
        $factor = 1;
        $timespan = 0;
        while (null !== ($part = array_pop($text))) {
            $timespan = $factor * $part;
            $factor *= 60;
        }
        return $timespan;
    }
}
