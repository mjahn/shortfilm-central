<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class TranslateModelsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translate:models {amount=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add missing translations to translatable models by using DEEPL';

    public function __construct(\App\Services\ModelTranslationService $modelTranslationService)
    {
        $this->service = $modelTranslationService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = $this->argument('amount');
        while ($amount > 0) {
            $movie = \App\Models\Movie::inRandomOrder()->first();
            echo $movie->slug ."\n";
            if ($this->service->handle($movie)) {
                $amount--;
            }
        }
    }
}
