<?php

namespace App\Console\Commands;

use App\Services\ExternalLinksService;
use Illuminate\Console\Command;

class ExternalLinkTitlesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'external:linktitles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update external links data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ExternalLinksService $service)
    {
        $this->service = $service;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->service->fetchLinkTitles();
    }
}
