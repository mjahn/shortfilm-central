<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CrawlerFestivalsDresdenCommand::class,
        Commands\CrawlerFestivalsLandshutCommand::class,
        Commands\SyncVideoDatabaseCommand::class,
        Commands\TranslateModelsCommand::class,
        Commands\ExternalLinkTitlesCommand::class,
        Commands\ExternalScreenshotsCommand::class,
        Commands\RegenerateResponsiveImagesCommand::class,
        Commands\CrawlerWebsiteTestkammerCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sync:drive:videos')->hourly();
        $schedule->command('sync:drive:images')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
