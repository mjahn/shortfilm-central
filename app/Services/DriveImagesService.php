<?php

namespace App\Services;

use App\Models\Movie;
use App\Models\MovieTranslation;
use Carbon\Carbon;
use Storage;

class DriveImagesService
{
	public static function syncToMediaLibrary($slug = null)
	{
		// fetch content of Google Drive folder
        $directories = collect(Storage::disk('images')->listContents('/'))->where('type', '=', 'dir');

        foreach ($directories as $directory) {
            $search = strtolower($directory['name']);

            $result = MovieTranslation::where('name', 'like', $search . '%')->first();
            if (!$result) {
                continue;
            }
            $files = collect(Storage::disk('images')->listContents($directory['path'], false))->where('type', '=', 'file');
            $movie = Movie::find($result->movie_id);
            foreach ($files as $image) {
                try {
                    Storage::disk('local')->put(
                        'temp/' . $image['path'], 
                        Storage::disk('images')->get($image['path'])
                    );
                } catch (\Exception $e) {
                    continue;
                }
                self::addMedia(
                    $movie,
                    Storage::disk('local')->path('temp/' . $image['path']), 
                    'images',
                    $image['filename']
                );
            }
        }
	}


    /**
     * Adds media to a model
     * 
     * @param Model $model      [description]
     * @param string $path        [description]
     * @param string $collection [description]
     * @param string $title      [description]
     */
    public static function addMedia($model, $path, $collection, $title) 
    {
        $title = substr($title, 0, 191);
        $filename = basename($path);

        // check if media from this path already exists and stop adding this media
        foreach ($model->getMedia($collection) as $media) {
            if ($media->file_name === $filename) {
                return;
            }
        }

        echo '#' . $model->id . ': found media ' .  $path . ' -> store to ' . $filename . "\n";
        try {
            $media = $model
                ->addMedia($path)
                ->usingFileName($filename)
                ->usingName($title)
                ->withResponsiveImages()
                ->toMediaCollection($collection);
        } catch (\Exception $e) {
            \Log::info('Exception', ['exception' => $e]);
        }
    }


}