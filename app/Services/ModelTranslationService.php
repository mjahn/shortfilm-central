<?php

namespace App\Services;

use hamburgscleanest\LaravelGuzzleThrottle\Facades\LaravelGuzzleThrottle;
use Illuminate\Database\Eloquent\Model;
use LanguageDetector\LanguageDetector;

class ModelTranslationService 
{
	
	public function handle(Model $model)
	{
		$updated = false;
		$deeplKey = '3027143b-d6bc-e286-e8b1-0c68f60c7604';
		$this->client = LaravelGuzzleThrottle::client([
			'base_uri' => 'https://api.deepl.com', 
			'cookies' => false,
			'debug' => false
		]);
		$existingLocale = null;
		$locales = [ 'de' => 0, 'en' => 0 ];
		if (!isset(class_implements($model)['Astrotomic\Translatable\Contracts\Translatable'])) {
			return false;
		}

		if (!isset($model->translatedAttributes)) {
			return false;
		}

		$translations = $model->getTranslationsArray();

		foreach ($translations as $locale => $data) {
			$value = $data['description'];

			// detected locale of the description
			$detectedLocale = LanguageDetector::detect($value)->getLanguage();
			if (!isset($locales[$detectedLocale])) {
				continue;
			}
			$locales[$detectedLocale] = 1;
			if ($detectedLocale === $locale) {
				continue;
			}

			// change locale of this translation to the detected locale of the description
			$updated = true;
			unset($translations[$locale]);
			$translations[$detectedLocale] = $data;
		}

		foreach ($locales as $locale => $count) {
			if ($count > 0) {
				$existingLocale = $locale;
				continue;
			}
			$updated = true;
		}

		if (!$updated || is_null($existingLocale)) {
			return false;
		}

		foreach ($locales as $locale => $count) {
			if ($count > 0) {
				continue;
			}

			$data = $translations[$existingLocale];
			$response = $this->client->request(
				'get',
				'/v2/translate?auth_key=' . $deeplKey . '&text=' . urlencode($data['description']) . '&source_lang=' . strtoupper($existingLocale) . '&target_lang=' . strtoupper($locale)
			);
			$response = json_decode($response->getBody(), true);
			$data['description'] = $response['translations'][0]['text'];

			$translations[$locale] = $data;
		}

		// dd($translations);
		foreach ($translations as $locale => $data) {
			foreach ($data as $key => $value) {
				$model->{$key . ':' . $locale} = $value;
			}
		}
		$model->save();
		return true;
	}
}