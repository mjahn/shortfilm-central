<?php

namespace App\Services\Externals;

abstract class AbstractExternalsService
{
	public function handle()
	{

	}

	abstract protected function getUrls();

	abstract protected function parseUrlContent($content);

}