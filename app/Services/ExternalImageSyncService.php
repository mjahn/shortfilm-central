<?php

namespace App\Services;

use App\Models\Movie;
use App\Models\External;
use App\Models\ExternalType;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AdapterInterface;

class ExternalImageSyncService
{
    public function __construct( $options = [])
    {
        \Log::channel('fs')->info('Initializing syncing');
        $this->options = $options;
        try {
            $this->storage = Storage::disk(config('sync.externals.target.fs'));
        } catch (\Exception $e) {
            \Log::channel('fs')->info('Error due initializing syncing', [ 'exception' => $e ]);
            return;
        }

        $max = max(1, min(200, config('sync.externals.amount')));

        $externals = External::where('external_type_id', '=', 1)->where('status', '=', 'new')->with(['movies', 'movies.eventPrograms', 'movies.events'])->where('value', '<>', 'http://films.filmfestivallife.com/')->take($max)->cursor();

        $bar = $this->options['output']->createProgressBar($max);

        foreach($externals as $external) {
            $bar->advance();
            $movie = $external->movies->first();

            if ($movie === null) {
                continue;
            }

            $filename = $this->generateFilename($external['value']);

            \Log::channel('fs')->info('Starting syncing of external', [ 'source' => $external['value'], 'target' => $filename ]);
            try {
                $this->addImage($movie, $external['value'], $filename);
            } catch (\Exception $e) {
                \Log::channel('fs')->info('Aborting syncing of external', [ 'source' => $external['value'], 'target' => $filename, 'exception' => $e ]);
                continue;
            }
            \Log::channel('fs')->info('Finishing syncing of external', [ 'source' => $external['value'], 'target' => $filename ]);
            $external->status = 'synced';
            $external->save();
        }

        $bar->finish();

    }

    private function generateFilename($url) {
        return hash_hmac('sha512', $url, 'image');
    }

    private function addImage(Movie $movie, $url, $filename)
    {
        $movie
            ->addMediaFromUrl($url)
            ->withResponsiveImages()
            ->usingName($filename)
            ->toMediaCollection('stills', 'media');
    }
}
