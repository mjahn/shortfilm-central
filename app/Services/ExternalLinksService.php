<?php

namespace App\Services;

use App\Models\External;
use Carbon\Carbon;
use App\Services\ApiFlashService;

class ExternalLinksService
{
    public function __construct(ApiFlashService $screenshotService)
    {
        $this->screenshotService = $screenshotService;
    }

	public function createLinkScreenshots()
	{
        $externals = External::whereHas('externalType', function ($query) {
            $query
                ->where('name', '=', 'link')
                ->orWhere('name', '=', 'review')
                ->orWhere('name', '=', 'interview');
        })
        ->doesntHave('media')
        ->get();

        foreach ($externals as $external) {
            $url = $this->screenshotService->getScreenshot($external->value);
            if ($url !== '') {
                $external->addMediaFromUrl($url)
                    ->withResponsiveImages()
                    ->toMediaCollection('screenshot');
            }
        }
	}

    public function fetchLinkTitles() 
    {
        $externals = External::where(function ($query) {
            $query->whereNull('name')->orWhere('name', '=', '');
        })->whereHas('externalType', function ($query) {
            $query->where('name', '=', 'link');
        })->get();

        foreach ($externals as $external) {
            $content = file_get_contents($external->value);
            $title = explode('</title>', explode('<title>', $content)[1])[0];
            $external->name = $title;
            $external->save();
        }
    }


}