<?php

namespace App\Services;

use App\Models\Movie;
use App\Models\External;
use App\Models\ExternalType;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\AdapterInterface;

class ExternalVideoSyncService
{
    public function __construct($options = [])
    {
        \Log::channel('fs')->info('Initializing syncing');
        $this->options = $options;
        try {
            $this->storage = Storage::disk(config('sync.externals.target.fs'));
        } catch (\Exception $e) {
            \Log::channel('fs')->info('Error due initializing syncing', [ 'exception' => $e ]);
            return;
        }

        $max = max(1, min(200, config('sync.externals.amount')));

        $videos = External::where('external_type_id', '=', 2)->where('status', '=', 'new')->with(['movies', 'movies.eventPrograms', 'movies.events'])->where('value', '<>', 'http://films.filmfestivallife.com/')->take($max)->cursor();

        $bar = $this->options['output']->createProgressBar($max);

        foreach($videos as $video) {
            $bar->advance();
            $movie = $video->movies->first();
            if ($movie === null) {
                continue;
            }
            $filename = $this->generateFilename(
                $movie->events->first()->name, 
                ($movie->eventPrograms->first() ? $movie->eventPrograms->first()->name : ''),
                $movie->name,
                $movie->year
            );

            \Log::channel('fs')->info('Starting syncing of external', [ 'source' => $video['value'], 'target' => $filename ]);
            try {
                $this->downloadVideo($video['value'], $filename);
            } catch (\Exception $e) {
                \Log::channel('fs')->info('Aborting syncing of fileexternal', [ 'source' => $video['value'], 'target' => $filename, 'exception' => $e ]);
                continue;
            }
            \Log::channel('fs')->info('Finishing syncing of external', [ 'source' => $video['value'], 'target' => $filename ]);
            $video->status = 'synced';
            $video->save();
        }

        $bar->finish();

    }

    private function downloadVideo($source, $target) {
        $stream = fopen($source, 'r');

        if ($this->storage->has($target)) {
            return;
        }
        
        $this->storage->writeStream($target, $stream, [
            'visibility' => AdapterInterface::VISIBILITY_PUBLIC
        ]);

        if (is_resource($stream)) {
            fclose($stream);
        }
    }

    private function generateFilename($event, $program, $title, $year) {
        $filename = $this->cleanFilename($event) . '/';
        $program = $this->cleanFilename($program);
        if ($program !== '') {
            $filename .= $program . '/';
        }
        $filename .= $this->cleanFilename($title) . ' (' . $year . ').mp4';
        return $filename;
    }

    private function cleanFilename($filename) {
        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;[]().
        // If you don't need to handle multi-byte characters
        // you can use preg_replace rather than mb_ereg_replace
        // Thanks @Łukasz Rysiak!
        $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
        // Remove any runs of periods (thanks falstro!)
        $filename = mb_ereg_replace("([\.]{2,})", '', $filename);

        return $filename;
    }
}
