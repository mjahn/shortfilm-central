<?php

namespace App\Services;

use App\Models\DriveVideo;
use Carbon\Carbon;

class DriveVideoService
{
	public static function syncToDatabase()
	{
		// fetch existing entries with id and basename
        $existingBasenames = DriveVideo::get([ 'id', 'basename' ])->pluck('id', 'basename')->toArray();

		// fetch content of Google Drive folder
        $files = \Storage::disk('google')->listContents('/', true);
        foreach ($files as $file) {
        	// ignore directories
        	if ($file['type'] !== 'file') {
        		continue;
        	}

        	// create or update an existing entry in the local database
            DriveVideo::updateOrCreate([
                'basename' => $file['basename']
            ], [
                'name' => $file['name'],
                'type' => $file['type'],
                'path' => $file['path'],
                'filename' => $file['filename'],
                'extension' => $file['extension'],
                'timestamp' => Carbon::createFromTimestamp($file['timestamp']),
                'mimetype' => $file['mimetype'],
                'size' => $file['size'],
                'dirname' => $file['dirname'],
            ]);

            // remove existing entry from existing entries list
            if (isset($existingBasenames[$file['basename']])) {
            	unset($existingBasenames[$file['basename']]);
            }

        }
        // destroy remaining entries in local database
        DriveVideo::destroy(array_values($existingBasenames));
	}
}