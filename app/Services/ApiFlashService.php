<?php
namespace App\Services;

use hamburgscleanest\LaravelGuzzleThrottle\Facades\LaravelGuzzleThrottle;

class ApiFlashService
{
	public function __construct()
	{
		$this->client = LaravelGuzzleThrottle::client([
			'base_uri' => 'https://api.apiflash.com',
			'debug' => false,
		]);
	}

	public function getScreenshot($url) 
	{
		$data = [
			'response_type' => 'json',
			'url' => $url,
			'format' => 'jpeg',
			'ttl' => 2592000,
			'access_key' => env('APIFLASH_KEY'),
		];
		$response = $this->client->request(
			'get',
			'/v1/urltoimage?' . http_build_query($data),
		);
		$response = json_decode((string) $response->getBody(), true);
		if (isset($response['url'])) {
			return $response['url'];
		}
		return '';
	}
}