<?php

namespace App\Services;

use hamburgscleanest\LaravelGuzzleThrottle\Facades\LaravelGuzzleThrottle;
use GuzzleHttp\Exception\ClientException;
use Log;

class HttpCrawlerService {

	public function __construct($baseUri, $beforeConfig = null)
	{
		$this->cookies = new \GuzzleHttp\Cookie\CookieJar();
		$this->client = LaravelGuzzleThrottle::client([
			'base_uri' => $baseUri, 
			'cookies' => true,
			'debug' => false
		]);

		if ($beforeConfig) {
			$this->before($beforeConfig);
		}
	}

	/**
	 * Method to call before 
	 * 
	 * @param  [type] $config [description]
	 * @return [type]         [description]
	 */
	protected function before ($config)
	{
		if (!isset($config)) {
			return;
		}

		if (isset($config['request'])) {
			$response = $this->client->request(
				'post',
				$config['request']['url'],
				$config['request']['options']
			);
			if (isset($config['response'])) {
				$this->handleResponse($response, $config['response']);
			}
		}
	}	

	/**
	 * Main crawler function
	 *
	 * Takes an array of URLs or a single URL and crawls the document behind it according to the steps-parameter. 
	 * Every step can contain a single action, that should be done with the document behind the URL.
	 * 
	 * @param  (array|string) 	$urls   	array or string with URL zto crawl
	 * @param  array 			$steps 	the step-wise configuration for crawling the document
	 * @return array        			array with the result-data, by default it contains an empty array
	 */
	public function crawl($urls, $steps, $debug = false)
	{
		$result = [
			'urls' => $urls
		];

		while (null !== ($url = array_shift($result['urls']))) {
			if ($debug) {
				echo 'Crawling URL ' . $url . "\n";
			}
			try {
				$response = $this->client->request(
					'get',
					$url,
				);
	        } catch (ClientException $exception) {
				Log::error('HttpCrawlerService: Error while fetching', [ 'exception' => $exception ]);
				continue;
			}

			Log::info('HttpCrawlerService: Start crawling', [ 'url' => $url ]);
			$content = (string) $response->getBody();
			foreach ($steps as $step) {
				if ($step['type'] === 'regex') {
					if ($res = preg_match_all($step['regex'], $content, $matches)) {
						$matches['content'] = $content;
						$content = $matches;
					} else {
						$content = [[], [], [], [], 'content' => $content];
					}
				}

				if ($step['type'] === 'dump') {
					print_r($content);
				}

				if ($step['type'] === 'split') {
					$content = explode($step['delimiter'], $content);
				}

				if ($step['type'] === 'shift') {
					array_shift($content);
				}

				if ($step['type'] === 'pop') {
					array_pop($content);
				}

				if ($step['type'] === 'exit') {
					exit;
				}

				if ($step['type'] === 'index') {
					foreach ($step['indexes'] as $key => $index) {
						if ($key === '*') {
							// Log::warn('Undefindex index', [ 'content' => $content, 'index' => $index ]);
							$content = $content[$index] ?? '';
							continue;
						}
						
						if ($index === '*') {
							if (is_array($content)) {
								if (!isset($result[$key])) {
									$result[$key] = [];
								}

								foreach ($content as $entry) {
									$result[$key][] = $entry;
								} 
							}
							continue;
						}
						if (!isset($result[$key])) {
							$result[$key] = [];
						}
						if (!isset($content[$index])) {
							continue;
						}
						if (is_array($content[$index])) {
							foreach ($content[$index] as $entry) {
								$result[$key][] = $entry;
							} 
						} else {
							$result[$key][] = $content[$index];
						}
					}
				}

				if ($step['type'] === 'strip_tags') {
					$content = strip_tags($content, $step['allowed_tags']);
				}
			}
			if ($debug) {
				echo 'result = ' . print_r($result, true);
			}
		}

		return $result;
	}	
}