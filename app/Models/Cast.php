<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cast extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'movie_id', 'person_id', 'cast_type_id'];

    /**
     * Get the movie  that owns the cast.
     */
    public function movies()
    {
        return $this->belongsToMany(Movie::class);
    }

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function castType()
    {
        return $this->belongsTo(CastType::class);
    }
}
