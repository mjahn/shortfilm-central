<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    public function collectables() 
    {
    	return $this->hasMany(Collectable::class);
    }
    
    /**
     * Get the users of the collection
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
