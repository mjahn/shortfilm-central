<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class EventTranslation extends Model
{
    use Searchable;

    /**
     * Disable timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Attributes that could be used in create and update-calls
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description' ];

    public function getSearchResult(): SearchResult
    {
        $event = Event::find($this->movie_id);

        return new SearchResult(
            $this,
            '<strong>' . $this->name . '</strong></a><br />' . substr($this->description, 0, 120) . '<a>',
            ($event ? $event->link() : '')
        );
    }
}