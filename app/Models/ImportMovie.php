<?php

namespace App\Models;

use Spatie\ViewModels\ViewModel;

class ImportMovie extends ViewModel
{

    /**
     * array with the moviedata
     * @var [type]
     */
    protected $movieData;
    /**
     * Create a new ImportMovie instance.
     *
     * @return void
     */
    public function __construct($movieData)
    {
        $this->movieData = $movieData;
    }

    public function __get($attribute) 
    {
        return $this->movieData[$attribute] ?? null;
    }

    public function __set($attribute, $value) 
    {
        return $this->movieData[$attribute] = $value;
    }
}
