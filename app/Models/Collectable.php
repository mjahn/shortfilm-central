<?php

namespace App\Models;

use App\Models\Event;
use App\Models\Movie;
use App\Models\DriveVideo as Video;
use Illuminate\Database\Eloquent\Model;

class Collectable extends Model
{
    /**
     * Attributes that could be used in create and update-calls
     *
     * @var array
     */
    public $fillable = [ 'collectable_id', 'collectable_type' ];

    /**
     * Get the collection, the collectable belongs to
     */
    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }

    /**
     * Get the movie that is assigned to this collectable
     */
    public function movie()
    {
        return $this->morphedBy(Movie::class, 'collectable');
    }

    /**
     * Get the video that is assigned to this collectable
     */
    public function video()
    {
        return $this->morphedBy(Video::class, 'collectable');
    }

    /**
     * Get the event that is assigned to this collectable
     */
    public function event()
    {
        return $this->morphedBy(Event::class, 'collectable');
    }


}
