<?php

namespace App\Models;

use App\Models\Movie;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use TomSchlick\Linkable\Linkable;

class DriveVideo extends Model
{
    use Searchable, Linkable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['timestamp'];

    protected $fillable = ['name', 'type', 'path', 'filename', 'extension', 'timestamp', 'mimetype', 'size', 'dirname', 'basename'];

    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        return 'https://drive.google.com/file/d/' . $this->basename . '/view';
    }

    public function getSearchResult(): SearchResult
    {
        return new SearchResult(
            $this,
            $this->name,
            $this->link()
        );
    }

    public static function findByMovie(Movie $movie)
    {
        $names = [];
        foreach ($movie->translations as $translation) {
            $names[] = $translation->name;
        }
        foreach ($names as $name) {
            $names[] = str_replace([
                ' and ', 
                ' und ',
            ], [
                ' & ', 
                ' & ',
            ], $name);
            $names[] = str_replace([
                ' & ', 
            ], [
                ' und ',
            ], $name);
        }
        foreach ($names as $name) {
            $names[] = htmlentities($name);
        }
        $names = array_unique ($names);

        $queryWhere = [];
        $queryData = [];
        foreach ($names as $name) {
            $queryWhere[] = 'lower(name) like lower(?)';
            $queryData[] = $name . ' (' . $movie->year .'%';
            $queryWhere[] = 'lower(name) like lower(?)';
            $queryData[] = $name . '%';
            $queryWhere[] = 'lower(name) like lower(?)';
            $queryData[] = '%(' . $name . ')%';
        }
        if (count($queryWhere) > 0) {
            return self::whereRaw(join(' or ', $queryWhere), $queryData)->get();
        } 
        return collect();
    }

    public function getPreviewImageAttribute()
    {
        return '<img src="https://drive.google.com/thumbnail?authuser=0&sz=w320&id=' . $this->path . '" />';
    }
}
