<?php
namespace App\Models;

use Pvtl\VoyagerPages\Page as BasePage;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use TomSchlick\Linkable\Linkable;

class Page extends BasePage
{
    use HasSlug, Linkable;

    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        if ($this->slug === '') {
            $this->generateSlug();
            $this->save();
        }
        return route('pages.' . $key, [
                'page' => $this->slug, // 'user_id' is the name of the parameter in the pages.* route group
                ] + $attr);
    }
    
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom([
                'title',
            ])
            ->saveSlugsTo('slug')
            // ->doNotGenerateSlugsOnCreate();
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}