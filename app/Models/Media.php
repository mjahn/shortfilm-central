<?php
namespace App\Models;

use Spatie\MediaLibrary\Models\Media as BaseMedia;

class Media extends BaseMedia
{
	public function movies()
	{
		return $this->belongsTo(Movie::class, 'model_id', 'id');
	}
}