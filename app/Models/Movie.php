<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Carbon\CarbonInterval;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Str;
use TomSchlick\Linkable\Linkable;

class Movie extends Model implements HasMedia, TranslatableContract, AuditableContract
{
    use HasSlug, SoftDeletes, HasMediaTrait, Translatable, Auditable, Linkable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Attributes that should be appended
     *
     * @var array
     */
    protected $appends = [ 'title', 'video', 'poster' ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude  = [ 'name', 'year', 'description', 'length' ];
    
    /**
     * Attributes that could be used in create and update-calls
     *
     * @var array
     */
    public $fillable = [ 'name', 'year', 'description', 'length', 'latest_version', 'country_code' ];

    /**
     * Attributes that could be tranlsated
     *
     * @var array
     */
    public $translatedAttributes = [ 'name', 'description' ];
    
    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        if ($this->slug === '') {
            $this->generateSlug();
            $this->save();
        }
        return route('movies.' . $key, [
                'movie' => $this->slug, // 'id' is the name of the parameter in the movies.* route group
                ] + $attr);
    }
    
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom([
                'name',
                'year',
                'director.fullname'
            ])
            ->saveSlugsTo('slug')
            // ->doNotGenerateSlugsOnCreate();
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the country of the movie
     */
    public function countries()
    {
        return $this->belongsToMany(Country::class, 'country_movie', 'movie_id', 'country_id');
    }
    
    /**
     * Get the categories of the movie
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
    
    /**
     * Get the casts of the movie
     */
    public function casts()
    {
        return $this->belongsToMany(Cast::class);
    }

    /**
     * Get the events of the movie
     */
    public function events()
    {
        return $this->belongsToMany(Event::class);
    }

    /**
     * Get the externals of the movie
     */
    public function externals()
    {
        return $this->morphMany(External::class, 'externable');
    }

    /**
     * Get the program of the movie
     */
    public function eventPrograms()
    {
        return $this->belongsToMany(EventProgram::class);
    }

    /**
     * Get all of the list items for the movie.
     */
    public function listables()
    {
        return $this->morphToMany(Listable::class, 'listable');
    }

    /**
     * Get the poster of the movie
     */
    public function getPosterAttribute()
    {
        $poster = $this->getFirstMedia('poster');
        if (is_null($poster)) {
            $poster = $this->getFirstMedia('images');
        }
        return $poster;
    }

    /**
     * Get the images of the movie
     */
    public function getImagesAttribute()
    {
        return $this->getMedia('images');
    }

    public function getTitleAttribute() 
    {
        return preg_replace_callback("/(&#[0-9]+;)/", function($m) { 
            return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); 
        }, $this->name); 
    }

    public function getVideoAttribute() 
    {
        $event = $this->events()->first();
        if (!$event) {
            return '';
        }
        $program = $event->programs()->first();
        if (!$program) {
            return '';
        }
        $filename = $this->cleanFilename($event->name) . '/';
        $program = $this->cleanFilename($program->name);
        if ($program !== '') {
            $filename .= $program . '/';
        }
        $filename .= $this->cleanFilename($this->title) . ' (' . $this->year . ').mp4';
        return $filename;
    }

    private function cleanFilename($filename) {
        // Remove anything which isn't a word, whitespace, number
        // or any of the following caracters -_~,;[]().
        // If you don't need to handle multi-byte characters
        // you can use preg_replace rather than mb_ereg_replace
        // Thanks @Łukasz Rysiak!
        $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
        // Remove any runs of periods (thanks falstro!)
        $filename = mb_ereg_replace("([\.]{2,})", '', $filename);

        return $filename;
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('images');
        $this->addMediaCollection('poster');
    }

    public function getLengthAttribute($value) 
    {
        $seconds = $value % 60;
        $minutes = floor(($value - $seconds) / 60);
        return $minutes . ':' . str_pad($seconds, 2, '0', STR_PAD_LEFT);
    }

    public function setLengthAttribute($value) 
    {
        $data = explode(':', $value);
        $multiplicator = 1;
        $length = 0;
        while ($value = array_pop($data)) {
            $length += intval($value) * $multiplicator;
            $multiplicator *= 60;
        }
        $this->attributes['length'] = $length;
    }

    public function getExternalVideosAttribute()
    {
        return $this->externals()->ofType('video')->get();
    }

    public function getExternalLinksAttribute()
    {
        return $this->externals()->ofType('link')->get();
    }

    public function getDriveVideosAttribute()
    {
        return DriveVideo::findByMovie($this);
    }
}
