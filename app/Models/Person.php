<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Sluggable\HasSlug;
use Laravel\Scout\Searchable;
use Spatie\Sluggable\SlugOptions;
use TomSchlick\Linkable\Linkable;

class Person extends Model implements HasMedia
{
    use HasSlug, SoftDeletes, Searchable, Linkable, HasMediaTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Attributes that could be used in the search
     *
     * @var array
     */
    protected $fillable = ['name', 'country_code'];

    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        return route('people.' . $key, [
                'person' => $this->slug, // 'user_id' is the name of the parameter in the events.* route group
                ] + $attr);
    }
    
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom([
                'name',
            ])
            ->saveSlugsTo('slug')
            // ->doNotGenerateSlugsOnCreate()
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the casts that owns the person.
     */
    public function casts()
    {
        return $this->hasMany(Cast::class);
    }

    /**
     * Get the movies of the person.
     */
    public function movies()
    {
        return $this->hasManyThrough(Movie::class, Cast::class);
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'country_code', 'iso_3166_2');
    }

    /**
     * Get the externals of the person
     */
    public function externals()
    {
        return $this->morphMany(External::class, 'externable');
    }

    public function getFullnameAttribute() {
        return $this->name;
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('images');
    }
}
