<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use TomSchlick\Linkable\Linkable;

class DriveFolder extends Model implements Searchable
{
    use Linkable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['timestamp'];

    protected $fillable = ['name', 'type', 'path', 'filename', 'extension', 'timestamp', 'mimetype', 'size', 'dirname', 'basename'];

    /**
     * Name of the search-type
     *
     * @var array
     */
    public $searchableType = 'Videodatenbank';
    
    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    
{        return 'https://drive.google.com/file/d/' . $this->basename . '/view';
    }

    public function getSearchResult(): SearchResult
    {
        return new SearchResult(
            $this,
            $this->name,
            $this->link()
        );
    }
}
