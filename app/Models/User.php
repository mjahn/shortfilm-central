<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use TomSchlick\Linkable\Linkable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable, Linkable;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        return route('users.' . $key, [
                'id' => $this->id, // 'user_id' is the name of the parameter in the events.* route group
                ] + $attr);
    }

    public function isAdmin() {
        return true;
    }
    
    public function selectPersonalData(PersonalDataSelection $personalDataSelection) {
        $personalDataSelection
            ->add('user.json', ['name' => $this->name, 'email' => $this->email])
            ->addFile(storage_path("avatars/{$this->id}.jpg")
            ->addFile('other-user-data.xml', 's3'));
    }
}
