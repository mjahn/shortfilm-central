<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class MovieTranslation extends Model
{
    use Searchable;

    /**
     * Disable timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Attributes that could be used in create and update-calls
     *
     * @var array
     */
    protected $fillable = [ 'name', 'description', 'movie_id' ];

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'movie_index';
    }

    /**
     * Get the movie of the translation
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }


}