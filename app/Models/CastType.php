<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CastType extends Model
{

    /**
     * Use timestamps in database for create and update
     * 
     * @var boolean
     */
    public $timestamps = false;

    protected $fillable = ['name'];

    /**
     * Get the casts that owns the cast-type.
     */
    public function casts()
    {
        return $this->hasMany(Cast::class);
    }
    //
}
