<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use TomSchlick\Linkable\Linkable;

class EventProgram extends Model
{
    use SoftDeletes, HasSlug, Linkable;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'start_at', 'end_at'];

    /**
     * Attributes that could be used in create and update-calls
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'event_id'];

    /**
     * create a sublink for a given route
     * 
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        if ($this->slug === '') {
            $this->generateSlug();
            $this->save();
        }
        // $this->load('event');
        return route('events.programs.' . $key, [
                'program' => $this->slug,
                'event' => $this->event->slug,
                ] + $attr);
    }
   
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom([
                'event.slug',
                'name',
            ])
            ->saveSlugsTo('slug')
            // ->doNotGenerateSlugsOnCreate();
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the movies that owns the event.
     */
    public function movies()
    {
        return $this->belongsToMany('App\Models\Movie');
    }

    /**
     * Get the movies that owns the event.
     */
    public function event()
    {
        return $this->belongsTo('App\Models\Event');
    }

    public function getLinkAttribute()
    {
        return '<a href="' . $this->link() . '">' . $this->name . '</a>';
    }
}
