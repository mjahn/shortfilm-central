<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $fillable = ['name'];

    /**
     * Get the users that owns the role.
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
    //
}
