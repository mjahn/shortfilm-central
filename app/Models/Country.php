<?php

namespace App\Models;

use Webpatser\Countries\Countries;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use TomSchlick\Linkable\Linkable;

class Country extends Countries
{
    use Linkable, HasSlug;

    /**
     * Use timestamps in database for create and update
     * 
     * @var boolean
     */
    public $timestamps = false;

    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        if ($this->slug === '') {
            $this->generateSlug();
            $this->save();
        }
        return route('countries.' . $key, [
                'id' => $this->slug,
                ] + $attr);
    }
    
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom([
                'name',
            ])
            ->saveSlugsTo('slug')
            ->doNotGenerateSlugsOnCreate()
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Get the movie  that owns the cast.
     */
    public function movies()
    {
        return $this->belongsToMany('App\Models\Movie');
    }

    public function people()
    {
        return $this->belongsTo('App\Models\Person');
    }

    public function events()
    {
        return $this->belongsTo('App\Models\Events');
    }
}
