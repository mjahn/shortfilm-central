<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class External extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    protected $fillable = ['value', 'externable_id', 'externable_type', 'externable_id', 'external_type_id', 'name'];
    
    /**
     * Get the externable that owns this external.
     */
    public function externable()
    {
        return $this->morphTo();
    }
    
    /**
     * Get the movies that owns the external.
     */
    public function externalType()
    {
        return $this->belongsTo(ExternalType::class);
    }

    public function getEmbedAttribute()
    {
        return str_replace(
            [
                'www.youtube.com/watch?v=', 
                'youtube.com/watch?v=',
                'www.vimeo.com/',
                'vimeo.com/', 
            ], [ 
                'youtube.com/embed/',
                'youtube.com/embed/',
                'player.vimeo.com/video/',
                'player.vimeo.com/video/',
            ], $this->value);
    }

    public function getPreviewImageAttribute()
    {
        $url = '';
        if (strpos($this->value, 'youtube.com') > 0) {
            $url = $this->getYoutubePreviewUrl($this->value);
        }

        if (strpos($this->value, 'vimeo.com') > 0) {
            $url = getVimeoPreviewUrl($this->value);
        }

        $url = trim($url);
        if ($url === '') {
            return '';
        }
        return '<img src="' . $url . '" />';
    }

    public function scopeOfType($query, $typeName)
    {
        $query->whereHas('externalType', function ($query) use ($typeName) {
            $query->where('name', '=', $typeName);
        });
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('screenshot');
    }

    /**
     * Grab the url of a publicly embeddable video hosted on vimeo
     * @param  str $video_url The "embed" url of a video
     * @return str            The url of the thumbnail, or false if there's an error
     */
    protected function getVimeoPreviewUrl($vimeoUrl)
    {
        if(!$vimeoUrl) {
            return false;
        }
        
        $data = json_decode( file_get_contents( 'https://vimeo.com/api/oembed.json?url=' . $vimeoUrl ) );
        
        if(!$data) {
            return false;
        }

        return $data->thumbnail_url;
    }

    /**
     * Grab the url of a publicly embeddable video hosted on vimeo
     * @param  str $video_url The "embed" url of a video
     * @return str            The url of the thumbnail, or false if there's an error
     */
    protected function getYoutubePreviewUrl($youtubeUrl)
    {
        if( !$youtubeUrl ) {
            return false;
        }

        $youtubeUrl = explode('&', $youtubeUrl)[0];
        $youtubeUrl = str_replace(
            [
                'www.youtube.com/watch?v=', 
                'youtube.com/watch?v=',
            ], 'img.youtube.com/vi/', $youtubeUrl);

        return $youtubeUrl . '/hqdefault.jpg';
    }
}
