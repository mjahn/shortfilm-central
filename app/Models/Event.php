<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use TomSchlick\Linkable\Linkable;

class Event extends Model implements HasMedia, TranslatableContract
{
    use SoftDeletes, HasMediaTrait, HasSlug, Linkable, Translatable;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'start_at', 'end_at'];

    protected $fillable = [ 'event_type_id', 'start_at', 'end_at', 'country_code'];

    protected $appends = [ 'title' ];

    /**
     * Attributes that could be tranlsated
     *
     * @var array
     */
    public $translatedAttributes = [ 'name', 'description' ];

    /**
     * create a sublink for a given route
     * @param  string $key  [description]
     * @param  array  $attr [description]
     * @return [type]       [description]
     */
    public function sublink(string $key, array $attr = []) : string
    {
        if ($this->slug === '') {
            $this->generateSlug();
            $this->save();
        }
        return route('events.' . $key, [
                'event' => $this->slug, // 'user_id' is the name of the parameter in the events.* route group
                ] + $attr);
    }

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom([
                'name',
            ])
            ->saveSlugsTo('slug')
            // ->doNotGenerateSlugsOnCreate();
            ->doNotGenerateSlugsOnUpdate();
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
        
    /**
     * Get the country of the event
     * @return Builder Query for the country
     */
    public function country()
    {
        return $this->hasOne('Webpatser\Countries\Countries');
    }

    /**
     * Get the event-type
     * @return Builder Query for the event-type
     */
    public function type()
    {
        return $this->hasOne('App\Models\EventType');
    }

    /**
     * Get the movies that owns the event.
     */
    public function movies()
    {
        return $this->belongsToMany('App\Models\Movie');
    }

    
    /**
     * Get the movies that owns the event.
     */
    public function eventType()
    {
        return $this->hasOne('App\Models\EventType');
    }

    /**
     * Get the movies that owns the event.
     */
    public function programs()
    {
        return $this->hasMany('App\Models\EventProgram');
    }

    /**
     * Get the startdate of the event
     * @return string
     */
    public function start() 
    {
        return $this->start_at->toFormattedDateString();
    }

    /**
     * Get the enddate of the event
     * @return string
     */
    public function end() 
    {
        return $this->end_at->toFormattedDateString();
    }

    /**
     * Get the title of the event
     * @return string
     */
    public function getTitleAttribute() 
    {
        return preg_replace_callback("/(&#[0-9]+;)/", function($m) { 
            return mb_convert_encoding($m[1], "UTF-8", "HTML-ENTITIES"); 
        }, $this->name); 
    }
}
