<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    /**
     * Disable timestamps
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Attributes that could be used in create and update-calls
     *
     * @var array
     */
    protected $fillable = [ 'name' ];
}