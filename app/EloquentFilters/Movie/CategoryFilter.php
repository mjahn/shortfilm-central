<?php

namespace App\EloquentFilters\Movie;

use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class CategoryFilter extends Filter
{
    /**
     * Apply the year condition to the query.
     *
     * @param Builder $builder
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply(Builder $builder, $value): Builder
    {
        return $builder->whereHas('categories', function ($query) use ($value) {
            $query
                ->whereHas('translations', function ($query) use ($value) {
                    $query->where('category_translations.name', '=', $value);
                })
                ->orWhere('categories.id', '=', $value);            
        });
    }
}