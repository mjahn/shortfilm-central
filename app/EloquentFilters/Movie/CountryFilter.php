<?php

namespace App\EloquentFilters\Movie;

use Fouladgar\EloquentBuilder\Support\Foundation\Contracts\Filter;
use Illuminate\Database\Eloquent\Builder;

class CountryFilter extends Filter
{
    /**
     * Apply the year condition to the query.
     *
     * @param Builder $builder
     * @param mixed   $value
     *
     * @return Builder
     */
    public function apply(Builder $builder, $value): Builder
    {
        return $builder->whereHas('countries', function ($query) use ($value) {
            $query
                ->where('countries.name', '=', $value)
                ->orWhere('countries.full_name', '=', $value)
                ->orWhere('countries.iso_3166_2', '=', $value)
                ->orWhere('countries.iso_3166_3', '=', $value);
        });
    }
}