<?php
namespace App\Filters;

use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MovieFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct($request);
    }
  
    public function name($term) {
        return $this->builder->where('movies.name', 'LIKE', "%$term%");
    }
  
    public function director($term) {
        return $this->builder->whereHas('casts', function ($query) use ($term) {
            return $query->whereHas('person', function ($query) use ($term) {
                return $query->where('name', 'LIKE', "%$term%");
            });
        });
    }
  
    public function year($term) {
        return $this->builder->where('year', '=', $term);
    }
    
    public function sort_year($type = null) {
        return $this->builder->orderBy('year', (!$type || $type == 'asc') ? 'desc' : 'asc');
    }
}