<?php

if (!function_exists('countryName'))
{
	function countryName($code)
	{
		static $countries = null;

		$locale = app()->getLocale();

		if (is_null($countries)) {
			$countries = (new \PeterColes\Countries\Maker())->keyValue($locale, 'code', 'label');
		}
		return $countries->where('code', '=', $code)->first()->label;
	}
}