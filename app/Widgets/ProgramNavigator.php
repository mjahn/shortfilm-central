<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Models\EventProgram;

class ProgramNavigator extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'program' => null
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        if (!isset($this->config['program'])) {
            return view('widgets.program_navigator', [
                'next' => null,
                'current' => $this->config['program'],
                'prev' => null,
            ]);
        }

        $programs = EventProgram::where('event_id', '=', $this->config['program']->event_id)->orderBy('name', 'asc')->get();
        $program = $programs->find($this->config['program']->id);

        return view('widgets.program_navigator', [
            'prev' => $programs->before($program),
            'current' => $program,
            'next' => $programs->after($program),
        ]);
    }
}
