<?php

namespace App\Widgets;

use App\Models\Cast;
use Arrilot\Widgets\AbstractWidget;

class RelatedMovies extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'movie' => null
    ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        if (!isset($this->config['movie'])) {
            return view('widgets.related_movies', [
                'movies' => []
            ]);
        }

        // get the cast of the movie
        $casts = $this->config['movie']->casts()->get();
        
        //  create a collection of movies with a weight
        $movies = [];
         
        //  for every cast-member get the movies 
        $peopleIds = [];
        foreach ($casts as $cast) {
            $peopleIds[] = $cast->person_id;
        }

        $personCasts = Cast::whereIn('person_id', $peopleIds)->with([ 'movies' ])->get();
        foreach ($personCasts as $personCast) {
            $personMovies = $personCast->movies()->get();
            foreach ($personMovies as $movie) {
                if ($this->config['movie']->id === $movie->id) {
                    continue;
                }
                //  create an index by movie-id or increment the existing one
                if (!isset($movies[$movie->id])) {
                    $movies[$movie->id] = $movie;
                    $movies[$movie->id]->weight = 0;
                }

                $movies[$movie->id]->weight++;
            }
        }
        $movies = collect(array_values($movies))->sortBy('weight')->values();

        return view('widgets.related_movies', compact('movies'));
    }
}
