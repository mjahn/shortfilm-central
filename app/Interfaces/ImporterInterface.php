<?php
namespace App\Interfaces;

interface ImporterInterface
{
    /**
     * Parse the imported data stream
     *
     * @param  string $handle Storage-URI
     * @return [type]         [description]
     */
    public function run($handle);
}
