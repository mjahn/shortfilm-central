<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {
    Route::resources([
        'countries' => 'CountryController',
        'events' => 'EventController',
        'movies' => 'MovieController',
        'people' => 'PersonController',
        'programs' => 'EventProgramController',
        'collections' => 'CollectionController',
        'users' => 'UserController',
        'categories' => 'CategoryController',
    ]);

    Route::get('/events/{event}/programs/', 'EventProgramController@index')->name('events.programs.index');
    Route::get('/events/{event}/programs/{program}', 'EventProgramController@show')->name('events.programs.show');

    Route::get('/users/{id}', 'UserController@show')->name('users.show');
    Route::get('/users/{id}/profile', 'UserController@profile')->name('users.profile');


    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::any('/search', 'SearchController@search')->name('search');
    Route::any('/search/autocomplete', 'SearchController@autocomplete')->name('search.autocomplete');

    Route::fallback('PageController@index');
    
    // Route::get('/home', 'PageController@show')->name('home');
    Auth::routes();
    
    try {
        $pages = \App\Models\Page::all();
      
        foreach ($pages as $page) {
            Route::get($page->slug, 'PageController@show')->name($page->slug);
        }
    } catch (\Exception $exception) {
        // do nothing
    }
});

//trigger the scheduler
Route::get('/f4d56s4fdfbd16sd4fsfafewdqhrj' , function(){
    Artisan::call('schedule:run');
    return 'OK';
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
