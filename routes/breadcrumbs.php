<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(__('navigation.home'), route('home'));
});

// Login
Breadcrumbs::for('auth.login', function ($trail) {
    $trail->parent('home');
    $trail->push(__('navigation.login'), route('login'));
});

// Home
Breadcrumbs::for('auth.register', function ($trail) {
    $trail->parent('home');
    $trail->push(__('navigation.register'), route('register'));
});

// Movie
Breadcrumbs::for('movies.index', function ($trail) {
    $trail->parent('home');
    $trail->push(__('navigation.movies'), route('movies.index'));
});

// Movies > [Movie]
Breadcrumbs::for('movies.show', function ($trail, App\Models\Movie $movie) {
    $trail->parent('movies.index');
    $trail->push($movie->title, $movie->link());
});

// Events
Breadcrumbs::for('events.index', function ($trail) {
    $trail->parent('home');
    $trail->push(__('navigation.events'), route('events.index'));
});

// Events > [Event]
Breadcrumbs::for('events.show', function ($trail, App\Models\Event $event) {
    $trail->parent('events.index');
    $trail->push($event->title, $event->link());
});

// Events > [Event] > Programs
Breadcrumbs::for('events.programs.index', function ($trail, App\Models\Event $event) {
    $trail->parent('events.show', $event);
    $trail->push(__('navigation.programs'), route('events.programs.index', $event));
});

// Events > [Event] > Programs > [Program]
Breadcrumbs::for('events.programs.show', function ($trail, App\Models\EventProgram $program, App\Models\Event $event) {
    $trail->parent('events.programs.index', $event);
    $trail->push($program->name, $program->link());
});

// Programs
Breadcrumbs::for('programs.index', function ($trail) {
    $trail->push(__('navigation.programs'), route('programs.index'));
});

// Programs > [Program]
Breadcrumbs::for('programs.show', function ($trail, App\Models\EventProgram $program) {
    $trail->parent('programs.index');
    $trail->push($program->name, $program->link());
});

// Peoples
Breadcrumbs::for('people.index', function ($trail) {
    $trail->parent('home');
    $trail->push(__('navigation.people'), route('people.index'));
});

// People > [Person]
Breadcrumbs::for('people.show', function ($trail, App\Models\Person $person) {
    $trail->parent('people.index');
    $trail->push($person->name, $person->link());
});

// Countries
Breadcrumbs::for('countries.index', function ($trail) {
    $trail->push(__('navigation.countries'), route('countries.index'));
});

// Countries > [Country]
Breadcrumbs::for('countries.show', function ($trail, App\Models\Country $country) {
    $trail->parent('countries.index');
    $trail->push($country->name, $country->link());
});

// [Page]
Breadcrumbs::for('pages.show', function ($trail, Pvtl\VoyagerPages\Page $page) {
    $trail->parent('home');
    $trail->push($page->title, $page->title);
});

// [User] > [Profile]
Breadcrumbs::for('users.profile', function ($trail, App\Models\User $user) {
    $trail->parent('home');
    $trail->push($user->name, $user->name);
});

// Collections
Breadcrumbs::for('collections.index', function ($trail) {
    $trail->parent('home');
    $trail->push(__('navigation.collections'), route('collections.index'));
});

// Collections > [Collection]
Breadcrumbs::for('collections.show', function ($trail, App\Models\Collection $collection) {
    $trail->parent('collections.index');
    $trail->push($collection->name, $collection->link());
});
