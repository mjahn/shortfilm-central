<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesSeeder::class);
        $this->call(EventTypesTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(ExternalTypesTableSeeder::class);
        $this->call(DriveVideoTableSeeder::class);
        $this->call(VoyagerDatabaseSeeder::class);
    }
}
