<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\EventType;

class EventTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        
        $this->seedEventTypes();

        Schema::enableForeignKeyConstraints();
    }

    protected function seedEventTypes() {
        $types = [
            'festival',
        ];
        //Empty the event_types table
        DB::table('event_types')->truncate(); 

        // Add all the event_types
        foreach ($types as $type) {
            DB::table('event_types')->insert([
                'name' => $type
            ]);
        }
    } 
}
