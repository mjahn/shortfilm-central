<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use App\Models\EventType;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        
        $this->seedEvents();

        Schema::enableForeignKeyConstraints();
    }

    protected function seedEvents() {
        $festivalTypeId = EventType::where('name', '=', 'festival')->first()->id;
        $events = [
            [
                'name' => '27. Bamberger Kurzfilmtage',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2017-01-23'),
                'end_at' => new \DateTime('2017-01-29'),
                'country_code' => 'DE',
            ],
            [
                'name' => '18. Landshuter Kurzfilmfestival',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2017-03-15'),
                'end_at' => new \DateTime('2017-03-20'),
                'country_code' => 'DE',
            ],
            [
                'name' => '29. Internationales Kurzfilm Festival Dresden',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2017-04-04'),
                'end_at' => new \DateTime('2017-04-09'),
                'country_code' => 'DE',
            ],
            [
                'name' => '28. Bamberger Kurzfilmtage',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2018-01-22'),
                'end_at' => new \DateTime('2018-01-28'),
                'country_code' => 'DE',
            ],
            [
                'name' => '19. Landshuter Kurzfilmfestival',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2018-03-14'),
                'end_at' => new \DateTime('2018-03-19'),
                'country_code' => 'DE',
            ],
            [
                'name' => '30. Internationales Kurzfilm Festival Dresden',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2018-04-17'),
                'end_at' => new \DateTime('2018-04-22'),
                'country_code' => 'DE',
            ],
            [
                'name' => '29. Bamberger Kurzfilmtage',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2019-01-21'),
                'end_at' => new \DateTime('2019-01-27'),
                'country_code' => 'DE',
            ],
            [
                'name' => '20. Landshuter Kurzfilmfestival',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2019-03-20'),
                'end_at' => new \DateTime('2019-03-25'),
                'country_code' => 'DE',
            ],
            [
                'name' => '31. Internationales Kurzfilm Festival Dresden',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2019-04-09'),
                'end_at' => new \DateTime('2019-04-14'),
                'country_code' => 'DE',
            ],
            [
                'name' => '61. DOK Leipzig',
                'description' => '',
                'event_type_id' => $festivalTypeId,
                'start_at' => new \DateTime('2018-10-29'),
                'end_at' => new \DateTime('2018-11-04'),
                'country_code' => 'DE',
            ],
        ];

        //Empty the events table
        DB::table('events')->truncate(); 

        // Add all the events
        foreach ($events as $event) {
            DB::table('events')->insert($event);
        }
    }
}
