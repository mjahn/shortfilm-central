<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ExternalTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        
        $this->seedExternalTypes();

        Schema::enableForeignKeyConstraints();
    }

    protected function seedExternalTypes() {
        $types = [
            'image',
            'video',
            'review',
        ];
        //Empty the event_types table
        DB::table('external_types')->truncate(); 

        // Add all the event_types
        foreach ($types as $type) {
            DB::table('external_types')->insert([
                'name' => $type
            ]);
        }
    } 
}
