<?php

use Illuminate\Database\Seeder;
use App\Services\DriveVideoService;

class DriveVideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DriveVideoService::syncToDatabase();
    }
}
