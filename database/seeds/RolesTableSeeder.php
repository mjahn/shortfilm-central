<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'admin' => 'Administrator',
        ];

        Schema::disableForeignKeyConstraints();
        
        //Empty the roles table
        DB::table('roles')->truncate();

        // Add all the roles
        foreach ($roles as $name => $displayName) {
            DB::table('roles')->insert([
                'name' => $name,
                'display_name' => $displayName,
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
