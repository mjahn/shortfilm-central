<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 256);
            $table->text('description');
            $table->string('slug', 256)->default('');
            $table->integer('event_type_id')->unsigned();
            $table->date('start_at');
            $table->date('end_at');
            $table->string('country_code', 2);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('country_code')->references('iso_3166_2')->on('countries')->onDelete('cascade');
            $table->foreign('event_type_id')->references('id')->on('event_types')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
