<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExternalsMorphable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('externals', function (Blueprint $table) {
            $table->integer('movie_id')->unsigned()->change();
            $table->string('externable_type')->after('movie_id');
            $table->renameColumn('movie_id', 'externable_id');
        });

        DB::table('externals')->update([
            'externable_type' => 'App\Models\Movie',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('externals', function (Blueprint $table) {
            //
        });
    }
}
