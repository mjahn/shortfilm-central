<?php

// @codeCoverageIgnoreStart

// example configuration
return [
    'cache' => [
        // Name of the configured driver in the Laravel cache config file / Also needs to be set when "no-cache" is set! Because it's used for the internal timers
        'driver'      => 'file', //\env('CACHE_DRIVER', 'file'),
        // Cache strategy: no-cache, cache, force-cache
        'strategy'    => 'force-cache',
        // TTL in minutes
        'ttl'         => 30 * 86400,
        // When this is set to false, empty responses won't be cached.
        'allow_empty' => false
    ],
    'rules' => [
        // host (including scheme)
        'https://api.apiflash.com' => [
            [
                // maximum number of requests in the given interval
                'max_requests'     => 100,
                // interval in seconds till the limit is reset
                'request_interval' => 86400  *30
            ],
        ],
        // host (including scheme)
        'https://www.db.filmfest-dresden.de' => [
            [
                // maximum number of requests in the given interval
                'max_requests'     => 20,
                // interval in seconds till the limit is reset
                'request_interval' => 10
            ],
            [
                // maximum number of requests in the given interval
                'max_requests'     => 100,
                // interval in seconds till the limit is reset
                'request_interval' => 120
            ]
        ],
        'https://api.deepl.com' => [
            [
                // maximum number of requests in the given interval
                'max_requests'     => 20,
                // interval in seconds till the limit is reset
                'request_interval' => 10
            ],
            [
                // maximum number of requests in the given interval
                'max_requests'     => 100,
                // interval in seconds till the limit is reset
                'request_interval' => 120
            ]
        ],
    ]
];

// @codeCoverageIgnoreEnd
