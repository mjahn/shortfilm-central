<?php

return [
	'festivals' => [
		'landshut' => [
			'host' => 'https://www.landshuter-kurzfilmfestival.de',
			'maps' => [
				'roles' => [
					'Regie' => 'director',
					'Produzent' => 'producer',
					'Drehbuch' => 'screenplay',
					'Kamera' => 'camera',
					'Schnitt' => 'editor',
					'Ton' => 'sound',
					'Music' => 'music',
					'Produktionsdesign' => 'production-design',
					'Besetzung' => 'actor',
				],
				'contact' => [
					'Email' => 'email',
					'Homepage' => 'website',
					'Produktionsfirma' => 'company',
				],
			],
			'festival' => [
				'urls' => [
					'/programm/alle-filme/',
				],
				'steps' => [
					[
						'type' => 'regex',
						'regex' => '/<a href="https:\/\/www\.landshuter-kurzfilmfestival\.de(\/programm\/[^"]+)">[^<]+<\/a>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'movie_urls' => 1,
						],
					],
				],
			],
			'movie' => [
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<div class="content-top">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<div id="comments" class="comments-area">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					// start regexes for movie-content
					[
						'type' => 'regex',
						'regex' => '/>([^<]+)<\/a><\/li><li class="active">([^<]+)<\/li>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'program' => 1,
							'title' => 2,
							'*' => 'content',
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<div class="entry-content">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div>([^,]*),\s*([^\d]+),\s*(\d\d\d\d),\s*([^\s]+)\s*min<\/div><div>([^<]*)<\/div>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'category' => 1,
							'country' => 2,
							'year' => 3,
							'length' => 4,
							'language' => 5,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/class="col-md-8"><img src="([^"]+)" alt="[^"]+"[^>]+><\/div>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'image' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div class="col-md-8 video"><iframe src="([^"]+)" /'
					],
					[
						'type' => 'index',
						'indexes' => [
							'trailer' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/class="col-md-4"><img src="([^"]+)" alt="[^"]+"[^>]+><\/div>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'poster' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div class="col-md-2">([^<]+)<\/div>\s*<div class="col-md-10">([^<]+)<\/div>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'roles' => 1,
							'names' => 2,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div style="margin:30px 0;">([^<]+)<\/div>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'description' => 1,
							'*' => 'content',
						],
					],
				],
			],
		],
		'dresden' => [
			'maps' => [
				'roles' => [
					'regie' => 'director',
					'director' => 'director',
					'producer' => 'producer',
					'screenplay' => 'screenplay',
					'camera' => 'camera',
					'editor' => 'editor',
					'sound' => 'sound',
					'cast' => 'actor',
				],
			],
			'host' => 'https://db.filmfest-dresden.de',
			'before' => [
				'request' => [
					'url' => '/',
					'method' => 'post',
					'options' => [
						'form_params' => [
							'name' => 'martinjahn@gmail.com',
							'pass' => 'rs8908957',
							'form_build_id' => 'form-4ccRblSUQYqPM2LeUQQ6lIlDXfcMKPPNpnqvKrCTqVk',
							'form_id' => 'user_login',
							'op' => 'Anmelden',
						],
					],
				],
			],
			'program' => [
				'url_pattern' => '/de/filmlibrary?page=',
				'urls' => [

				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<!-- /.block -->',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => ' view-group-left ',
					],
					[
						'type' => 'shift',
					],
					[
						'type' => 'index',
						'indexes' => [
							'movies' => '*',
						],
					],
				],
			],
			'movies_extended' => [
				'url_pattern' => '/jquery_ajax_load/get/view/film-library-detail/',
				'urls' => [

				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<video'
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<!-- /.block -->'
					],
					[
						'type' => 'index',
						'indexes' => [
							'movies' => 0,
						],
					],
				],
			],
		],
		'dok_leipzig' => [
			'host' => 'https://filmarchiv.dok-leipzig.de',
			'maps' => [
				'roles' => [
					'Regie' => 'director',
					'Produzent' => 'producer',
					'Produktion' => 'producer',
					'Animation' => 'animation',
					'Drehbuch' => 'screenplay',
					'Buch' => 'screenplay',
					'Kamera' => 'camera',
					'Schnitt' => 'editor',
					'Ton' => 'sound',
					'Music' => 'music',
					'Produktionsdesign' => 'production-design',
					'Besetzung' => 'actor',
				],
				'contact' => [
					'Email' => 'email',
					'Homepage' => 'website',
					'Produktionsfirma' => 'company',
				],
			],
			'program-list' => [
				'urls' => [
					'/de',
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => "[{'value':0,'text':'Alle Filmreihen'}",
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => "{'SelectedIndexChanged'",
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					[
						'type' => 'regex',
						'regex' => '/(?:,{\'value\':(\d+),\'text\':\'([^\']+)\'})/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'id' => 1,
						],
					],
				],
			],
			'program' => [
				'url_pattern' => '/de?&section=',
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<h1 id="mainContent">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => '1',
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<script',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => '0',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/(\d+)\s*min.\s*<\/strong>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'time' => 1,
							'*' => 'content',					[
							],
						],
					],
					[
						'type' => 'regex',
						'regex' => '/href="\/de\/film\/\?ID=(\d+)/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'ids' => 1,
							'*' => 'content',					[
							],
						],
					],
				],
			],
			'movie' => [
				'url_pattern' => '/de/film/?ID=',
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<div id="panFilm">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<div id="divAnotation">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					// start regexes for movie-content
					[
						'type' => 'regex',
						'regex' => '/<h1 id="mainContent">\s*([^<]+?)\s+<\/h1>\s+<p class="film-info-top">\s+<label style="font-weight: bold;">\s+(\d\d\d\d DOK Leipzig)\s-\s([^<]+?)\s+<\/label>\s*<br \/>\s*<label>\s*([^<]+?)\s*<\/label>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'title' => 1,
							'event' => 2,
							'program' => 3,
							'meta' => 4,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<img class="lazy film-item-img	data-original="([^"]+)"/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'image' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div class=(?:\'|")credit_detail_text(?:\'|")>([^\:]+): (.*?)<\/div>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'key' => 1,
							'value' => 2,
							'*' => 'content',
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<div Class="detail_text">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '</div>',
					],
					[
						'type' => 'index',
						'indexes' => [
							'description' => 0,
							'*' => 1,
						],
					],
				],
			],
		],
		'cottbus' => [
			'host' => 'http://www.filmfestivalcottbus.de',
			'maps' => [
				'roles' => [
					'Regie' => 'director',
					'Produzent' => 'producer',
					'Produktion' => 'producer',
					'Co-Produktion' => 'producer',
					'Animation' => 'animation',
					'Drehbuch' => 'screenplay',
					'Buch' => 'screenplay',
					'Kamera' => 'camera',
					'Schnitt' => 'editor',
					'Ton' => 'sound',
					'Music' => 'music',
					'Musik' => 'music',
					'Produktionsdesign' => 'production-design',
					'Besetzung' => 'actor',
					'Darsteller' => 'actor',
				],
				'contact' => [
					'Email' => 'email',
					'Homepage' => 'website',
					'Produktionsfirma' => 'company',
				],
			],
			'events' => [
				'urls' => [
					'/de/festival/archiv.html',
				],
				'steps' => [
					[
						'type' => 'regex',
						'regex' => '/<li class="" data-value="(\d+)" onclick="[^"]+">(\d\d\d\d)<\/li>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'ids' => 1,
							'years' => 2,
						],
					],
				],
			],
			'event' => [
				'url_pattern' => '/de/festival/archiv/movies.html?filter[vintage]=',
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => ' href="/de/festival/archiv/movie/',
					],
					[
						'type' => 'shift',
					],
					[
						'type' => 'index',
						'indexes' => [
							'movies' => '*',
						],
					],
				],
			],
			'movie' => [
				'url_pattern' => '/de/film/?ID=',
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<div class="movieinfos col-md-6 col-md-offset-3">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<footer id="footer">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					// start regexes for movie-content
					[
						'type' => 'regex',
						'regex' => '/class="section">Sektion: (.*?)<\/span>\s+<h1 class="titleorg">\s*(.*?)<\/h1>\s*<h2 class="titlelng">\s*(.*?)<\/h2>\s*<\/div>\s*<div[^>]+>\s*<div[^>]+>\s*<div class="director"><strong>(.*?)<\/strong>\s*<\/div>\s*<div class="produktion">(.*?),\s([\d\s\/-]*),\s(\d+)\sMin<\/div>\s*<div class="teaser">(.*?)<\/div>/is',
					],
					[
						'type' => 'index',
						'indexes' => [
							'program' => 1,
							'title' => 2,
							'title_german' => 3,
							'director' => 4,
							'country' => 5,
							'year' => 6,
							'length' => 7,
							'teaser' => 8,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<img src="(\/images\/movie_images\/[^"]+)/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'image' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<a href="(.*?)" class="anyButton">Trailer jetzt ansehen<\/a>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'trailer' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div>\s*<b>(.*?)<\/b><br\s*\/>\s*(.*?)\s*<\/div>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'key' => 1,
							'value' => 2,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<!-- Infos zu Regie mit Bild -->\s*<img src="(\/images\/director_images\/[^"]+)"/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'director_image' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<p><b class="bitter">\s*(.*?)\s*<\/b>\s*-\s*(.*?)\s*<\/p>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'director' => 1,
							'director_description' => 2,
							'*' => 'content',
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<!-- Film Text Lang in jeweiliger Sprache -->'
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '</div>',
					],
					[
						'type' => 'index',
						'indexes' => [
							'description' => 0,
						],
					],
				],
			],
		],
		'genrenale' => [
			'host' => 'https://genrenale.de',
			'maps' => [
				'roles' => [
					'Regie' => 'director',
					'Produzent' => 'producer',
					'Drehbuch' => 'screenplay',
					'Kamera' => 'camera',
					'Schnitt' => 'editor',
					'Ton' => 'sound',
					'Music' => 'music',
					'Produktionsdesign' => 'production-design',
					'Besetzung' => 'actor',
				],
				'contact' => [
					'Email' => 'email',
					'Homepage' => 'website',
					'Produktionsfirma' => 'company',
				],
			],
			'festival' => [
				'urls' => [
					'/category/filme-2019/',
				],
				'steps' => [
					[
						'type' => 'regex',
						'regex' => '/<a href="https:\/\/www\.landshuter-kurzfilmfestival\.de(\/programm\/[^"]+)">[^<]+<\/a>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'movie_urls' => 1,
						],
					],
				],
			],
			'movie' => [
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<div class="content-top">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<div id="comments" class="comments-area">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					// start regexes for movie-content
					[
						'type' => 'regex',
						'regex' => '/>([^<]+)<\/a><\/li><li class="active">([^<]+)<\/li>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'program' => 1,
							'title' => 2,
							'*' => 'content',
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<div class="entry-content">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div>([^,]*),\s*([^\d]+),\s*(\d\d\d\d),\s*([^\s]+)\s*min<\/div><div>([^<]*)<\/div>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'category' => 1,
							'country' => 2,
							'year' => 3,
							'length' => 4,
							'language' => 5,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/class="col-md-8"><img src="([^"]+)" alt="[^"]+"[^>]+><\/div>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'image' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div class="col-md-8 video"><iframe src="([^"]+)" /'
					],
					[
						'type' => 'index',
						'indexes' => [
							'trailer' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/class="col-md-4"><img src="([^"]+)" alt="[^"]+"[^>]+><\/div>/'
					],
					[
						'type' => 'index',
						'indexes' => [
							'poster' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div class="col-md-2">([^<]+)<\/div>\s*<div class="col-md-10">([^<]+)<\/div>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'roles' => 1,
							'names' => 2,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<div style="margin:30px 0;">([^<]+)<\/div>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'description' => 1,
							'*' => 'content',
						],
					],
				],
			],
		],
	],
	'databases' => [
		'dresden' => [
			'host' => 'https://www.filmuniversitaet.de',
			'index' => [
				'urls' => [
					'/film/filmdatenbank/',
				],
				'steps' => [
					[
						'type' => 'regex',
						'regex' => '/pagination__link--next"\s*href="([^"]+")/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'urls' => 1,
						],
					],
					[
						'type' => 'regex',
						'regex' => '(\/film\/filmdatenbank\/film\/fdb\/[^"]+)',
					],
					[
						'type' => 'index',
						'indexes' => [
							'movies' => 1,
						],
					],
				],
			],
			'movies' => [
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'regex' => '<main class="page__content" id="inhalt">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'regex' => '</main>',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					[
						'type' => 'regex',
						'regex' => '(\/film\/filmdatenbank\/film\/fdb\/[^"]+)',
					],
					[
						'type' => 'index',
						'indexes' => [
							'title' => 1,
						],
					],
				],
			],
		],
	],
	'websites' => [
		'testkammer' => [
			'host' => 'https://testkammer.com',
			'reviews' => [
				'urls' => [
					'/archiv/kurzfilme/'
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<div class="entry-content">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<!-- .entry-content -->',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<a href="(https{0,1}:\/\/testkammer\.com\/\d{4}\/\d{2}\/\d{2}\/[^"]+)"[^>]+>([^<]+)<\/a>[^\(]*\((\d{4})\)/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'url' => 1,
							'title' => 2,
							'year' => 3
						],
					],
				],
			],
			'review' => [
				'urls' => [],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => 'Quellen:</span>',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '</ul>',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<a href="([^"]+)"[^>]+>([^<]+)</',
					],
					[
						'type' => 'index',
						'indexes' => [
							'links' => 1,
							'titles' => 2,
						],
					],
				],
			],
			'interviews' => [
				'urls' => [
					'/archiv/interviews/'
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<div class="entry-content">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<!-- .entry-content -->',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<a href="(https:\/\/testkammer.com\/\d{4}\/\d{2}\/\d{2}\/[^"]+)"[^>]+>([^<]+)<\/a>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'url' => 1,
							'title' => 2,
						],
					],
				],
			],
			'interview' => [
				'urls' => [
				],
				'steps' => [
					[
						'type' => 'split',
						'delimiter' => '<header class="entry-header">',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 1,
						],
					],
					[
						'type' => 'split',
						'delimiter' => '<!-- .entry-content -->',
					],
					[
						'type' => 'index',
						'indexes' => [
							'*' => 0,
						],
					],
	 				[
						'type' => 'regex',
						'regex' => '/ data-large-file="([^?]+)\?{0,1}[^"]*"/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'image' => 1,
							'*' => 'content',
						],
					],
	 				[
						'type' => 'regex',
						'regex' => '/ data-image-title="([^"]+)"/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'title' => 1,
							'*' => 'content',
						],
					],
					[
						'type' => 'regex',
						'regex' => '/<h1 class="entry-title">([^<]+)<\/h1>/',
					],
					[
						'type' => 'index',
						'indexes' => [
							'name' => 1,
						],
					],
				],
			],
		]
	]
];