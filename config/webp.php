<?php
return [
    'converters' => [
        'cwebp', 'vips', 'imagick', 'gmagick', 'imagemagick', 'graphicsmagick', 'wpc', 'ewww', 'gd'
    ],

    // Any available options can be set here, they dribble down to all converters.
    'metadata' => 'all',

    // To override for specific converter, you can prefix with converter id:
    'cwebp-metadata' => 'exif',

    // This can for example be used for setting ewww api key:
    'ewww-api-key' => 'your-ewww-api-key-here',

    // As an alternative to prefixing, you can use "converter-options" to set a whole bunch of overrides in one go:
    'converter-options' => [        
        'wpc' => [
            'crypt-api-key-in-transfer' => true,
            'api-key' => 'my dog is white',
            'api-url' => 'https://example.com/wpc.php',
            'api-version' => 1,
        ],
    ],
    'png' => [
        'encoding' => 'auto',    /* Try both lossy and lossless and pick smallest */
        'near-lossless' => 60,   /* The level of near-lossless image preprocessing (when trying lossless) */
        'quality' => 85,         /* Quality when trying lossy. It is set high because pngs is often selected to ensure high quality */
    ],
    'jpeg' => [
        'encoding' => 'auto',     /* If you are worried about the longer conversion time, you could set it to "lossy" instead (lossy will often be smaller than lossless for jpegs) */
        'quality' => 'auto',      /* Set to same as jpeg (requires imagick or gmagick extension, not necessarily compiled with webp) */
        'max-quality' => 80,      /* Only relevant if quality is set to "auto" */
        'default-quality' => 75,  /* Fallback quality if quality detection isnt working */
    ]
];
