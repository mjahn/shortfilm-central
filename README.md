shortfilm-central


https://github.com/spatie/laravel-personal-data-export
https://github.com/spatie/laravel-csp
https://github.com/spatie/laravel-glide
https://github.com/spatie/laravel-searchable
https://github.com/spatie/laravel-model-status
https://github.com/spatie/data-transfer-object
https://github.com/spatie/laravel-activitylog -> Logging of changes in database
https://github.com/spatie/schema-org -> Generating schema.org data for movies, persons and events
https://github.com/barryvdh/laravel-translation-manager
https://github.com/spatie/laravel-view-models
https://docs.astrotomic.info/laravel-translatable/

https://sampo.co.uk/blog/using-enums-in-laravel
https://github.com/mtvs/eloquent-approval
https://github.com/yabhq/laravel-scout-mysql-driver

https://dev.to/povilaskorop/laravel-approve-new-registered-users-from-administrator-3nbh

* Widgets are done with https://github.com/arrilot/laravel-widgets

https://zurb.com/playground/foundation-icon-fonts-3
https://github.com/mohammad-fouladgar/eloquent-builder

https://github.com/chriskonnertz/deeply

https://github.com/rosell-dk/webp-convert